# 💉 Serum Automated Market Maker

## 📜 Additional Market Definitions

SAMM loads market definitions from [`serum-js`’s defined list of market](https://github.com/project-serum/serum-js/blob/master/src/markets.json). That covers most situations.

But one of the best things about decentralized exchanges is that they are permissionless. You can create your own markets, set up your own markets, and you don’t need `serum-js`’s (or anyone else’s) permission to do it.


## 📖 Telling SAMM About A Market

**Note:** If your market uses one or two tokens that are unknown to `serum-js`, you’ll need to follow the [instructions to add token definitions](AdditionalTokens.md) as well.

If `serum-js` is out of date, or if you have created a market and want SAMM to work with it, you need to tell SAMM:
* The market’s symbol, and
* The market’s address, and
* The market’s programId

You combine these into a single string, separated by a colon (the address and programId should be base58-encoded). So if you want to add a market with the symbol _MNB/LKJ_ and it has an address of _EXfcikncccci22rWh4PKD7ZKPwzPUYNYvqTnpv87GXZM_ and a program ID of _5JqGvPGh3eCEdFCNjvHcATF2536Anpo5Y333Z4WzmhjT_, you would combine them to create a string `"MNB/LKJ:EXfcikncccci22rWh4PKD7ZKPwzPUYNYvqTnpv87GXZM:5JqGvPGh3eCEdFCNjvHcATF2536Anpo5Y333Z4WzmhjT"`.

Then you just pass that string using the parameter `--additionalMarket` to whatever SAMM command you’re running, like:
```
$ /var/sammd/bin/samm orders --market MNB/LKJ --additionalMarket "MNB/LKJ:EXfcikncccci22rWh4PKD7ZKPwzPUYNYvqTnpv87GXZM:5JqGvPGh3eCEdFCNjvHcATF2536Anpo5Y333Z4WzmhjT"
```

That should be all you need to do to make SAMM aware of the _MNB/LKJ_ market, but you will need to pass it as a parameter every time.

And again, if you need to make SAMM aware of the _MNB_ or _LKJ_ tokens you’ll need to follow the [instructions to add token definitions](AdditionalTokens.md) as well. For example, as a more complete command line, if you want to add a market based on two tokens and `serum-js` knows neither the tokens nor the market, you would use command-line such as the following:
```
$ /var/sammd/bin/samm orders --market MNB/LKJ --additionalMarket "MNB/LKJ:EXfcikncccci22rWh4PKD7ZKPwzPUYNYvqTnpv87GXZM:5JqGvPGh3eCEdFCNjvHcATF2536Anpo5Y333Z4WzmhjT" --additionalToken "MNB:AHJ8b36o3D2XA9GVr6C7gyERiF2D796oLKqxSFXX9Bqx" --additionalToken "LKJ:8N3Z8czoU9oZvt95iAVPysBX1cJXF6wHSeHA52nHh8wu"
```
This specifies one additional market (using the `--additionalMarket` parameter) and two additional tokens (using the `--additionalToken` parameter). This should then be sufficient for SAMM to load all the relevant metadata for the market and tokens.


## 📖 Telling SAMM About Multiple Markets

The process for telling SAMM about multiple markets is the same as for a single market. All SAMM commands can take multiple `--additionalMarket` parameters, so you just specify that parameter as many times as you need. For example, to add the market _MNB/LKJ_ with address _EXfcikncccci22rWh4PKD7ZKPwzPUYNYvqTnpv87GXZM_ and program ID _5JqGvPGh3eCEdFCNjvHcATF2536Anpo5Y333Z4WzmhjT_, and the market _ABC/DEF_ with address _2TGeAecg36YhYDKUZCtEpVY1Fp9QdkYvcKFg8tFgADrh_ and program ID _Hw89DN6UMW4iFcNVyRorwhhVVb75dVRSiBMuHEeKr2x9_, you would use:
```
$ /var/sammd/bin/samm orders --market MNB/LKJ --additionalMarket "MNB/LKJ:EXfcikncccci22rWh4PKD7ZKPwzPUYNYvqTnpv87GXZM:5JqGvPGh3eCEdFCNjvHcATF2536Anpo5Y333Z4WzmhjT" --additionalMarket "ABC/DEF:2TGeAecg36YhYDKUZCtEpVY1Fp9QdkYvcKFg8tFgADrh:Hw89DN6UMW4iFcNVyRorwhhVVb75dVRSiBMuHEeKr2x9"
```
You can specify `--additionalMarket` as many times as you need. Again, you will need to pass those parameters every time you want SAMM to be aware of those markets.


## 📖 Telling SAMM About Multiple Markets Using A File

It’s possible to add markets to a file and have SAMM load that, instead of having to always specify markets on the command line.

Depending on your runtime environment, that may be a better option for you. Unfortunately docker makes this a bit harder.

The file itself should be in the same format as used by `serum-js`. For example, to add the two markets used above, the file should look like:
```
[
    {
        "address": "EXfcikncccci22rWh4PKD7ZKPwzPUYNYvqTnpv87GXZM",
        "name": "MNB/LKJ",
        "programId": "5JqGvPGh3eCEdFCNjvHcATF2536Anpo5Y333Z4WzmhjT",
        deprecated: false
    },
    {
        "address": "2TGeAecg36YhYDKUZCtEpVY1Fp9QdkYvcKFg8tFgADrh",
        "name": "ABC/DEF",
        "programId": "Hw89DN6UMW4iFcNVyRorwhhVVb75dVRSiBMuHEeKr2x9",
        deprecated: false
    }
]
```
It should be an array of objects, and each object should have a `name` (the market symbol), an `address` (the DEX address for the market), a `programId` (the address of the program in Solana) and a `deprecated` flag (deprecated markets will not be loaded, so this should usually be `false`).

The file should be called `markets.json` and it should be in the current directory. Alternatively, you can call the file whatever you like, place it wherever you like, and pass the `--additionalMarketFile` parameter with the full path to the file, e.g. `--additionalMarketFile /var/sammd/additionalMarkets.json`.

Docker causes problems here because the additional file needs to be mapped into SAMM’s execution context inside docker. If you use the `sammd` scripts from the [QuickStart](QuickStart.md) you’ll need to update them with the additional line `-v /var/sammd/markets.json:/home/node/app/markets.json \\`, for example:
```
$ cat << EOF > /var/sammd/bin/sammd
#! /usr/bin/env bash
docker run --rm -d --name sammd \\
    -v /var/sammd/wallet.json:/home/node/app/wallet.json \\
    -v /var/sammd/markets.json:/home/node/app/markets.json \\
    -v /var/sammd/auditlogs:/home/node/app/auditlogs \\
    opinionatedgeek/samm:v0.1 \\
    \$@
docker logs --follow sammd
EOF
```
And:
```
$ cat << EOF > /var/sammd/bin/samm
#! /usr/bin/env bash
docker run --rm -it --name=samm \\
    -v /var/sammd/wallet.json:/home/node/app/wallet.json \\
    -v /var/sammd/markets.json:/home/node/app/markets.json \\
    -v /var/sammd/auditlogs:/home/node/app/auditlogs \\
    opinionatedgeek/samm:v0.1 \\
    \$@
EOF
```


## 📖 Using Combinations Of The Above

The load order may be important if you’re using any of the above approaches, so to be clear, market definitions are loaded in the following order:

1. Market definitions are loaded from serum-js.
2. Market definitions are then loaded from any custom market JSON file. If a symbol or address clashes, _the existing symbol or address is overwritten by the new value loaded from the file_.
3. Market definitions are then loaded from the command line. If a symbol or address clashes, _the existing symbol or address is overwritten by the new value loaded from the command line_.

Or put another way, market definitions specified on the command line takes priority over market definitions specified in a markets.json file, which in turn takes priority over market definitions from serum.js.