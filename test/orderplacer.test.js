"use strict";

const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const orderplacer = require("../lib/orderplacer.js");

describe("OrderPlacer", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    describe("SimulationOrderPlacer", () => {
        it("Constructor should succeed with no parameters", () => {
            const orderPlacer = new orderplacer.SimulationOrderPlacer();
            expect(orderPlacer.constructor.name).to.equal("SimulationOrderPlacer");
        });

        it("placeOrders() should not place orders", async () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderPlacer = new orderplacer.SimulationOrderPlacer();
            await orderPlacer.placeOrders(fakes.orders(101, 102));
            expect(market.typedPlaceOrder.called).to.be.false;
            expect(context.connection.waitFor.called).to.be.false;
        });
    });

    describe("AsyncOrderPlacer", () => {
        it("Constructor should succeed with valid parameters", () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderPlacer = new orderplacer.AsyncOrderPlacer(context, market);
            expect(orderPlacer.constructor.name).to.equal("AsyncOrderPlacer");
        });

        it("Constructor should fail with invalid context", () => {
            const shouldThrow = () => new orderplacer.AsyncOrderPlacer({}, fakes.market());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const shouldThrow = () => new orderplacer.AsyncOrderPlacer(fakes.context(), {});
            expect(shouldThrow).to.throw();
        });

        it("placeOrders() should place orders but not wait", async () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderPlacer = new orderplacer.AsyncOrderPlacer(context, market);
            await orderPlacer.placeOrders(fakes.orders(101, 102));
            expect(market.typedPlaceOrder.called).to.be.true;
            expect(market.typedPlaceOrder.calledTwice).to.be.true;
            expect(context.connection.waitFor.called).to.be.false;
        });
    });

    describe("AsyncWaitingOrderPlacer", () => {
        it("Constructor should succeed with valid parameters", () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderPlacer = new orderplacer.AsyncWaitingOrderPlacer(context, market);
            expect(orderPlacer.constructor.name).to.equal("AsyncWaitingOrderPlacer");
        });

        it("Constructor should fail with invalid context", () => {
            const shouldThrow = () => new orderplacer.AsyncWaitingOrderPlacer({}, fakes.market());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const shouldThrow = () => new orderplacer.AsyncWaitingOrderPlacer(fakes.context(), {});
            expect(shouldThrow).to.throw();
        });

        it("placeOrders() should place orders but not wait", async () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderPlacer = new orderplacer.AsyncWaitingOrderPlacer(context, market);
            await orderPlacer.placeOrders(fakes.orders(101, 102));
            expect(market.typedPlaceOrder.called).to.be.true;
            expect(market.typedPlaceOrder.calledTwice).to.be.true;
            expect(context.connection.waitFor.called).to.be.true;
            expect(context.connection.waitFor.calledTwice).to.be.true;
        });
    });

    describe("SyncOrderPlacer", () => {
        it("Constructor should succeed with no parameters", () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderPlacer = new orderplacer.SyncOrderPlacer(context, market);
            expect(orderPlacer.constructor.name).to.equal("SyncOrderPlacer");
        });

        it("Constructor should fail with invalid context", () => {
            const shouldThrow = () => new orderplacer.SyncOrderPlacer({}, fakes.market());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const shouldThrow = () => new orderplacer.SyncOrderPlacer(fakes.context(), {});
            expect(shouldThrow).to.throw();
        });

        it("placeOrders() should place orders and wait", async () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderPlacer = new orderplacer.SyncOrderPlacer(context, market);
            await orderPlacer.placeOrders(fakes.orders(101, 102));
            expect(market.typedPlaceOrder.called).to.be.true;
            expect(market.typedPlaceOrder.calledTwice).to.be.true;
            expect(context.connection.waitFor.called).to.be.true;
        });
    });
});