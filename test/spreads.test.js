"use strict";

const Big = require("big.js");
const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const order = require("../lib/order.js");
const serum = require("@project-serum/serum");
const sinon = require("sinon");
const spreads = require("../lib/spreads.js");

describe("Spreads", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    const FAKE_BIDS = [
        {
            price: 1,
            size: 1,
            side: "buy"
        },
        {
            price: 2,
            size: 1,
            side: "buy"
        },
        {
            price: 3,
            size: 1,
            side: "buy"
        },
        {
            price: 4,
            size: 1,
            side: "buy"
        },
        {
            price: 5,
            size: 1,
            side: "buy"
        }
    ];
    const FAKE_ASKS = [
        {
            price: 7,
            size: 1,
            side: "sell"
        },
        {
            price: 8,
            size: 1,
            side: "sell"
        },
        {
            price: 9,
            size: 1,
            side: "sell"
        },
        {
            price: 10,
            size: 1,
            side: "sell"
        },
        {
            price: 11,
            size: 1,
            side: "sell"
        }
    ];

    describe("Spread", () => {
        it("Constructor should succeed with valid parameters", () => {
            const actual = new spreads.Spread(
                fakes.FAKE_BASE_TOKEN,
                fakes.FAKE_QUOTE_TOKEN,
                new Big(5),
                FAKE_BIDS,
                new Big(7),
                FAKE_ASKS,
                new Big(6)
            );
            expect(actual.isSpread).to.be.true;
            expect(actual.constructor.name).to.equal("Spread");
        });

        it("Constructor should fail with invalid base token", () => {
            const shouldThrow = () => new spreads.Spread({},
                fakes.FAKE_QUOTE_TOKEN,
                new Big(5),
                FAKE_BIDS,
                new Big(7),
                FAKE_ASKS,
                new Big(6));
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid quote token", () => {
            const shouldThrow = () => new spreads.Spread(
                fakes.FAKE_BASE_TOKEN,
                {},
                new Big(5),
                FAKE_BIDS,
                new Big(7),
                FAKE_ASKS,
                new Big(6)
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructed object should have correct properties", () => {
            const actual = new spreads.Spread(
                fakes.FAKE_BASE_TOKEN,
                fakes.FAKE_QUOTE_TOKEN,
                new Big(5),
                FAKE_BIDS,
                new Big(7),
                FAKE_ASKS,
                new Big(6)
            );
            expect(actual.bestBid.eq(5)).to.be.true;
            expect(actual.bestAsk.eq(7)).to.be.true;
            expect(actual.mid.eq(6)).to.be.true;
            expect(actual.bids.length).to.equal(5);
            expect(actual.asks.length).to.equal(5);
            expect(actual.baseToken.symbol).to.equal("FAKEBASE");
            expect(actual.quoteToken.symbol).to.equal("FAKEQUOTE");
        });

        it("toString() should show proper spread data", () => {
            const actual = new spreads.Spread(
                fakes.FAKE_BASE_TOKEN,
                fakes.FAKE_QUOTE_TOKEN,
                new Big(5),
                FAKE_BIDS,
                new Big(7),
                FAKE_ASKS,
                new Big(6)
            );
            expect(actual.toString()).to.equal("« 5 / 7 FAKEQUOTE »");
        });
    });

    describe("MarketSpreadFetcher", () => {
        function spyMarket() {
            const fake = new serum.Market({
                "accountFlags": {
                    "initialized": true,
                    "market": true
                }
            });
            fake.base = "BTC";
            fake.quote = "USDT";
            fake.typedLoadBids = sinon.fake.returns(order.Order.fromSerumArray(FAKE_BIDS));
            fake.typedLoadAsks = sinon.fake.returns(order.Order.fromSerumArray(FAKE_ASKS));

            return fake;
        }

        it("Constructor should succeed with valid parameters", () => {
            const actual = new spreads.MarketSpreadFetcher(fakes.context(), spyMarket());
            expect(actual.isSpreadFetcher).to.be.true;
            expect(actual.constructor.name).to.equal("MarketSpreadFetcher");
        });

        it("Constructor should fail with invalid context", () => {
            const shouldThrow = () => new spreads.MarketSpreadFetcher("invalid", spyMarket());
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const shouldThrow = () => new spreads.MarketSpreadFetcher(fakes.context(), "invalid");
            expect(shouldThrow).to.throw();
        });

        it("fetchSpread() should return proper spread data", async () => {
            const context = fakes.context();
            const market = spyMarket();
            const actual = new spreads.MarketSpreadFetcher(context, market);
            const spread = await actual.fetchSpread();
            expect(spread.bestBid.eq(5)).to.be.true;
            expect(spread.bestAsk.eq(7)).to.be.true;
            expect(spread.mid.eq(6)).to.be.true;
            expect(spread.bids.length).to.equal(5);
            expect(spread.asks.length).to.equal(5);
            expect(spread.baseToken.symbol).to.equal("BTC");
            expect(spread.quoteToken.symbol).to.equal("USDT");
            expect(spread.toString()).to.equal("« 5 / 7 USDT »");
        });
    });
});