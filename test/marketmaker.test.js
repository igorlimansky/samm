"use strict";

const Big = require("big.js");
const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const marketmaker = require("../lib/marketmaker.js");
const order = require("../lib/order.js");
const ordercanceller = require("../lib/ordercanceller.js");
const orderfetcher = require("../lib/orderfetcher.js");
const orderplacer = require("../lib/orderplacer.js");
const ordertracker = require("../lib/ordertracker.js");
const orderwaiter = require("../lib/orderwaiter.js");

describe("MarketMaker", () => {
    log.setLevels({
        trace: false,
        print: false,
        warn: false
    });

    describe("MarketMaker", () => {
        it("Constructor should successfully construct with valid parameters", () => {
            const orderBuilder = new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                fakes.balanceSettler(),
                fakes.fillReporter(),
                fakes.orderBuilder(),
                new orderfetcher.SimulationOrderFetcher(),
                new orderplacer.SimulationOrderPlacer(),
                new ordertracker.OrderTracker(),
                new ordercanceller.SimulationOrderCanceller(),
                new orderwaiter.SimulationOrderWaiter()
            );
            expect(orderBuilder.constructor.name).to.equal("MarketMaker");
        });

        it("Constructor should throw with invalid balance fetcher", () => {
            const shouldThrow = () => new marketmaker.MarketMaker(
                "invalid",
                fakes.balanceSettler(),
                fakes.fillReporter(),
                fakes.orderBuilder(),
                new orderfetcher.SimulationOrderFetcher(),
                new orderplacer.SimulationOrderPlacer(),
                new ordertracker.OrderTracker(),
                new ordercanceller.SimulationOrderCanceller(),
                new orderwaiter.SimulationOrderWaiter()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid balance settler", () => {
            const shouldThrow = () => new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                "invalid",
                fakes.balanceSettler(),
                fakes.fillReporter(),
                fakes.orderBuilder(),
                new orderfetcher.SimulationOrderFetcher(),
                new orderplacer.SimulationOrderPlacer(),
                new ordertracker.OrderTracker(),
                new ordercanceller.SimulationOrderCanceller(),
                new orderwaiter.SimulationOrderWaiter()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid fill reporter", () => {
            const shouldThrow = () => new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                fakes.balanceSettler(),
                "invalid",
                fakes.orderBuilder(),
                new orderfetcher.SimulationOrderFetcher(),
                new orderplacer.SimulationOrderPlacer(),
                new ordertracker.OrderTracker(),
                new ordercanceller.SimulationOrderCanceller(),
                new orderwaiter.SimulationOrderWaiter()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid order builder", () => {
            const shouldThrow = () => new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                fakes.balanceSettler(),
                fakes.fillReporter(),
                "invalid",
                new orderfetcher.SimulationOrderFetcher(),
                new orderplacer.SimulationOrderPlacer(),
                new ordertracker.OrderTracker(),
                new ordercanceller.SimulationOrderCanceller(),
                new orderwaiter.SimulationOrderWaiter()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid order fetcher", () => {
            const shouldThrow = () => new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                fakes.balanceSettler(),
                fakes.fillReporter(),
                fakes.orderBuilder(),
                "invalid",
                new orderplacer.SimulationOrderPlacer(),
                new ordertracker.OrderTracker(),
                new ordercanceller.SimulationOrderCanceller(),
                new orderwaiter.SimulationOrderWaiter()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid order placer", () => {
            const shouldThrow = () => new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                fakes.balanceSettler(),
                fakes.fillReporter(),
                fakes.orderBuilder(),
                new orderfetcher.SimulationOrderFetcher(),
                "invalid",
                new ordertracker.OrderTracker(),
                new ordercanceller.SimulationOrderCanceller(),
                new orderwaiter.SimulationOrderWaiter()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid order tracker", () => {
            const shouldThrow = () => new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                fakes.balanceSettler(),
                fakes.fillReporter(),
                fakes.orderBuilder(),
                new orderfetcher.SimulationOrderFetcher(),
                new orderplacer.SimulationOrderPlacer(),
                "invalid",
                new ordercanceller.SimulationOrderCanceller(),
                new orderwaiter.SimulationOrderWaiter()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid order canceller", () => {
            const shouldThrow = () => new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                fakes.balanceSettler(),
                fakes.fillReporter(),
                fakes.orderBuilder(),
                new orderfetcher.SimulationOrderFetcher(),
                new orderplacer.SimulationOrderPlacer(),
                new ordertracker.OrderTracker(),
                "invalid",
                new orderwaiter.SimulationOrderWaiter()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid order waiter", () => {
            const shouldThrow = () => new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                fakes.balanceSettler(),
                fakes.fillReporter(),
                fakes.orderBuilder(),
                new orderfetcher.SimulationOrderFetcher(),
                new orderplacer.SimulationOrderPlacer(),
                new ordertracker.OrderTracker(),
                new ordercanceller.SimulationOrderCanceller(),
                "invalid"
            );
            expect(shouldThrow).to.throw();
        });

        it("processTick() should not throw", async () => {
            const marketMaker = new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                fakes.balanceSettler(),
                fakes.fillReporter(),
                fakes.orderBuilder(),
                new orderfetcher.SimulationOrderFetcher(),
                new orderplacer.SimulationOrderPlacer(),
                new ordertracker.OrderTracker(),
                new ordercanceller.SimulationOrderCanceller(),
                new orderwaiter.SimulationOrderWaiter()
            );

            await marketMaker.processTick();
        });

        it("processTick() should place orders", async () => {
            const orders = [
                new order.Order(
                    order.OrderSide.BUY,
                    fakes.owner(),
                    fakes.tokenAccount(),
                    order.OrderType.POSTONLY,
                    new Big(98),
                    new Big(0.1),
                    null,
                    new Big(11)
                ),
                new order.Order(
                    order.OrderSide.SELL,
                    fakes.owner(),
                    fakes.tokenAccount(),
                    order.OrderType.POSTONLY,
                    new Big(102),
                    new Big(0.1),
                    null,
                    new Big(12)
                )
            ];
            const orderPlacer = fakes.orderPlacer();
            const marketMaker = new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                fakes.balanceSettler(),
                fakes.fillReporter(),
                fakes.orderBuilder(orders),
                new orderfetcher.SimulationOrderFetcher(),
                orderPlacer,
                new ordertracker.OrderTracker(),
                new ordercanceller.SimulationOrderCanceller(),
                new orderwaiter.SimulationOrderWaiter()
            );

            await marketMaker.processTick();

            expect(fakes.ordersMatch(orders[0], orderPlacer.placedOrders[0])).to.be.true;
            expect(fakes.ordersMatch(orders[1], orderPlacer.placedOrders[1])).to.be.true;
        });

        it("processTick() should settle when placing new orders", async () => {
            const orders = [
                new order.Order(
                    order.OrderSide.BUY,
                    fakes.owner(),
                    fakes.tokenAccount(),
                    order.OrderType.POSTONLY,
                    new Big(98),
                    new Big(0.1),
                    null,
                    new Big(11)
                ),
                new order.Order(
                    order.OrderSide.SELL,
                    fakes.owner(),
                    fakes.tokenAccount(),
                    order.OrderType.POSTONLY,
                    new Big(102),
                    new Big(0.1),
                    null,
                    new Big(12)
                )
            ];
            const orderPlacer = fakes.orderPlacer();
            const balanceSettler = fakes.balanceSettler();
            const marketMaker = new marketmaker.MarketMaker(
                fakes.balanceFetcher(),
                balanceSettler,
                fakes.fillReporter(),
                fakes.orderBuilder(orders),
                new orderfetcher.SimulationOrderFetcher(),
                orderPlacer,
                new ordertracker.OrderTracker(),
                new ordercanceller.SimulationOrderCanceller(),
                new orderwaiter.SimulationOrderWaiter()
            );

            expect(balanceSettler.settle.called).to.be.false;
            await marketMaker.processTick();
            expect(fakes.ordersMatch(orders[0], orderPlacer.placedOrders[0])).to.be.true;
            expect(balanceSettler.settle.called).to.be.true;
            expect(balanceSettler.settle.calledOnce).to.be.true;
        });
    });
});