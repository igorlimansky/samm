"use strict";

const {
    expect
} = require("chai");
const Big = require("big.js");
const BN = require("bn.js");
const log = require("../lib/log.js");
const numbers = require("../lib/numbers.js");

describe("Numbers", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    it("Zero should equal new Big(0)", () => {
        expect(numbers.Zero.eq(new Big(0))).to.be.true;
    });

    it("Zero should equal 0", () => {
        expect(numbers.Zero.eq(0)).to.be.true;
    });

    it("Zero should not equal 1", () => {
        expect(numbers.Zero.eq(1)).to.be.false;
    });

    it("One should equal new Big(1)", () => {
        expect(numbers.One.eq(new Big(1))).to.be.true;
    });

    it("One should equal 1", () => {
        expect(numbers.One.eq(1)).to.be.true;
    });

    it("One should not equal 2", () => {
        expect(numbers.One.eq(2)).to.be.false;
    });

    it("BNZero should equal new Big(0)", () => {
        expect(numbers.BNZero.eq(new BN(0))).to.be.true;
    });

    it("BNZero should not equal 0", () => {
        expect(numbers.BNZero.eq(0)).to.be.false;
    });

    it("BNZero should not equal 1", () => {
        expect(numbers.BNZero.eq(1)).to.be.false;
    });

    it("BNOne should equal new Big(1)", () => {
        expect(numbers.BNOne.eq(new BN(1))).to.be.true;
    });

    it("BNOne should not equal 1", () => {
        expect(numbers.BNOne.eq(1)).to.be.false;
    });

    it("BNOne should not equal 2", () => {
        expect(numbers.BNOne.eq(2)).to.be.false;
    });

    it("createBigFromNumberAndDecimals(1, 2) should give 0.01", () => {
        expect(Number(numbers.createBigFromNumberAndDecimals(1, 2))).to.equal(0.01);
    });

    it("createBigFromNumberAndDecimals(1, 3) should not give 0.01", () => {
        expect(Number(numbers.createBigFromNumberAndDecimals(1, 3))).to.not.equal(0.01);
    });

    it("createBigFromNumberAndDecimals(7, 0) should give 7", () => {
        expect(Number(numbers.createBigFromNumberAndDecimals(7, 0))).to.equal(7);
    });

    it("createBigFromBNAndDecimals(1, 2) should give 0.01", () => {
        expect(Number(numbers.createBigFromBNAndDecimals(new BN(1), 2))).to.equal(0.01);
    });

    it("createBigFromBNAndDecimals(1, 3) should not give 0.01", () => {
        expect(Number(numbers.createBigFromBNAndDecimals(new BN(1), 3))).to.not.equal(0.01);
    });

    it("createBigFromBNAndDecimals(7, 0) should give 7", () => {
        expect(Number(numbers.createBigFromBNAndDecimals(new BN(7), 0))).to.equal(7);
    });

    it("isZero() should return true for 0", () => {
        expect(numbers.isZero(0)).to.be.true;
    });

    it("isZero() should return true for Zero", () => {
        expect(numbers.isZero(numbers.Zero)).to.be.true;
    });

    it("isZero() should return false for One", () => {
        expect(numbers.isZero(numbers.One)).to.be.false;
    });

    it("isZero() should return true for BN Zero", () => {
        expect(numbers.isZero(numbers.BNZero)).to.be.true;
    });

    it("isZero() should return false for BN One", () => {
        expect(numbers.isZero(numbers.BNOne)).to.be.false;
    });

    it("isZeroOrUndefined() should return true for 0", () => {
        expect(numbers.isZeroOrUndefined(0)).to.be.true;
    });

    it("isZeroOrUndefined() should return true for Zero", () => {
        expect(numbers.isZeroOrUndefined(numbers.Zero)).to.be.true;
    });

    it("isZeroOrUndefined() should return false for One", () => {
        expect(numbers.isZeroOrUndefined(numbers.One)).to.be.false;
    });

    it("isZeroOrUndefined() should return true for BN Zero", () => {
        expect(numbers.isZeroOrUndefined(numbers.BNZero)).to.be.true;
    });

    it("isZeroOrUndefined() should return false for BN One", () => {
        expect(numbers.isZeroOrUndefined(numbers.BNOne)).to.be.false;
    });

    it("isZeroOrUndefined() should return true for undefined", () => {
        // eslint-disable-next-line no-undefined
        expect(numbers.isZeroOrUndefined(undefined)).to.be.true;
    });

    it("isBN() should return true for BNZero", () => {
        expect(numbers.isBN(numbers.BNZero)).to.be.true;
    });

    it("isBN() should return true for BNOne", () => {
        expect(numbers.isBN(numbers.BNOne)).to.be.true;
    });

    it("isBN() should return true for arbitrary BN", () => {
        expect(numbers.isBN(new BN(7))).to.be.true;
    });

    it("isBN() should return false for Zero", () => {
        expect(numbers.isBN(numbers.Zero)).to.be.false;
    });

    it("isBN() should return false for One", () => {
        expect(numbers.isBN(numbers.One)).to.be.false;
    });

    it("isBN() should return false for arbitrary Big", () => {
        expect(numbers.isBN(new Big(7))).to.be.false;
    });

    it("isBN() should return false for undefined", () => {
        // eslint-disable-next-line no-undefined
        expect(numbers.isBN(undefined)).to.be.false;
    });

    it("isBN() should return false for string", () => {
        expect(numbers.isBN("7")).to.be.false;
    });

    it("negate() should return -1 for One", () => {
        expect(numbers.negate(numbers.One).eq(new Big(-1))).to.be.true;
    });

    it("negate() should return 1 for -1", () => {
        expect(numbers.negate(new Big(-1)).eq(numbers.One)).to.be.true;
    });

    it("negate() should return -7 for 7", () => {
        expect(numbers.negate(new Big(7)).eq(new Big(-7))).to.be.true;
    });

    it("negate() should return 2.5 for -2.5", () => {
        expect(numbers.negate(new Big(-2.5)).eq(new Big(2.5))).to.be.true;
    });

    it("negate() should return Zero for Zero", () => {
        expect(numbers.isZero(numbers.negate(numbers.Zero))).to.be.true;
    });
});