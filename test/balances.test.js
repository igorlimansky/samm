"use strict";

const balances = require("../lib/balances.js");
const Big = require("big.js");
const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const numbers = require("../lib/numbers.js");

describe("BalanceFetcher", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    describe("SyncBalanceFetcher", () => {
        const market = fakes.market("BTC", "USDT");
        const owner = fakes.owner();
        const context = fakes.context();

        it("Constructor should successfully construct with valid parameters", () => {
            const bal = new balances.SyncBalanceFetcher(
                context,
                market,
                owner,
                [fakes.tokenAccount()],
                [fakes.tokenAccount()]
            );
            expect(bal.constructor.name).to.equal("SyncBalanceFetcher");
            expect(bal instanceof balances.SyncBalanceFetcher).to.be.true;
            expect(bal.isBalanceFetcher).to.be.true;
        });

        it("Constructor should fail with invalid context", () => {
            const shouldThrow = () => new balances.SyncBalanceFetcher(
                "invalid",
                market,
                owner,
                [fakes.tokenAccount()],
                [fakes.tokenAccount()]
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const shouldThrow = () => new balances.SyncBalanceFetcher(
                context,
                "invalid",
                owner,
                [fakes.tokenAccount()],
                [fakes.tokenAccount()]
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid owner", () => {
            const shouldThrow = () => new balances.SyncBalanceFetcher(
                context,
                market,
                "invalid",
                [fakes.tokenAccount()],
                [fakes.tokenAccount()]
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid base token account list", () => {
            const shouldThrow = () => new balances.SyncBalanceFetcher(
                context,
                market,
                owner,
                "invalid",
                [fakes.tokenAccount()]
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid quote token account list", () => {
            const shouldThrow = () => new balances.SyncBalanceFetcher(
                context,
                market,
                owner,
                [fakes.tokenAccount()],
                "invalid"
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should return object with Tokens", () => {
            const bal = new balances.SyncBalanceFetcher(
                context,
                market,
                owner,
                [fakes.tokenAccount()],
                [fakes.tokenAccount()]
            );
            expect(bal.baseToken.symbol).to.equal("BTC");
            expect(bal.quoteToken.symbol).to.equal("USDT");
        });

        it("fetchBalance() should return a complete balance object", async () => {
            const bal = new balances.SyncBalanceFetcher(
                context,
                market,
                owner,
                [fakes.tokenAccount()],
                [fakes.tokenAccount()]
            );

            const result = await bal.fetchBalance();
            expect(result.actualTotal.base.value.toString()).to.equal("1");
            expect(result.actualTotal.quote.value.toString()).to.equal("1");
            expect(result.total.base.value.toString()).to.equal("1");
            expect(result.total.quote.value.toString()).to.equal("1");
            expect(result.settled.base.value.toString()).to.equal("1");
            expect(result.settled.quote.value.toString()).to.equal("1");
            expect(result.unsettled.base.value.toString()).to.equal("0");
            expect(result.unsettled.quote.value.toString()).to.equal("0");
            expect(result.toString()).to.equal("« « 1 BTC », « 1 USDT » »");
        });
    });

    describe("FixedBalanceFetcher", () => {
        const inner = fakes.balanceFetcher();

        it("Constructor should successfully construct with valid parameters", () => {
            const bal = new balances.FixedBalanceFetcher(inner, new Big("2"), new Big("20000"));
            expect(bal.constructor.name).to.equal("FixedBalanceFetcher");
            expect(bal instanceof balances.FixedBalanceFetcher).to.be.true;
            expect(bal.isBalanceFetcher).to.be.true;
        });

        it("Constructor should fail with invalid inner balance fetcher", () => {
            const shouldThrow = () => new balances.FixedBalanceFetcher("invalid", new Big("2"), new Big("20000"));
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid base token balance", () => {
            const shouldThrow = () => new balances.FixedBalanceFetcher(inner, "invalid", new Big("20000"));
            expect(shouldThrow).to.throw();
        });

        it("Constructor should successfully construct with undefined base token balance", () => {
            // eslint-disable-next-line no-undefined
            const bal = new balances.FixedBalanceFetcher(inner, undefined, new Big("20000"));
            expect(bal.constructor.name).to.equal("FixedBalanceFetcher");
            expect(bal instanceof balances.FixedBalanceFetcher).to.be.true;
            expect(bal.isBalanceFetcher).to.be.true;
        });

        it("Constructor should fail with invalid quote token balance", () => {
            const shouldThrow = () => new balances.FixedBalanceFetcher(inner, new Big("2"), "invalid");
            expect(shouldThrow).to.throw();
        });

        it("Constructor should successfully construct with undefined quote token balance", () => {
            // eslint-disable-next-line no-undefined
            const bal = new balances.FixedBalanceFetcher(inner, new Big("2"), undefined);
            expect(bal.constructor.name).to.equal("FixedBalanceFetcher");
            expect(bal instanceof balances.FixedBalanceFetcher).to.be.true;
            expect(bal.isBalanceFetcher).to.be.true;
        });

        it("Constructor should successfully construct with undefined base and quote token balances", () => {
            // eslint-disable-next-line no-undefined
            const bal = new balances.FixedBalanceFetcher(inner, undefined, undefined);
            expect(bal.constructor.name).to.equal("FixedBalanceFetcher");
            expect(bal instanceof balances.FixedBalanceFetcher).to.be.true;
            expect(bal.isBalanceFetcher).to.be.true;
        });

        it("fetchBalance() should return specified balance values", async () => {
            const bal = new balances.FixedBalanceFetcher(inner, new Big("2"), new Big("20000"));

            const result = await bal.fetchBalance();
            expect(result.actualTotal.base.value.toString()).to.equal("1");
            expect(result.actualTotal.quote.value.toString()).to.equal("1");
            expect(result.total.base.value.toString()).to.equal("2");
            expect(result.total.quote.value.toString()).to.equal("20000");
            expect(result.settled.base.value.toString()).to.equal("2");
            expect(result.settled.quote.value.toString()).to.equal("20000");
            expect(result.unsettled.base.value.toString()).to.equal("0");
            expect(result.unsettled.quote.value.toString()).to.equal("0");
            expect(result.toString()).to.equal("« « 2 BTC », « 20000 USDT » »");
        });

        it("fetchBalance() should only update base when only base specified", async () => {
            const bal = new balances.FixedBalanceFetcher(inner, new Big("2"));

            const result = await bal.fetchBalance();
            expect(result.actualTotal.base.value.toString()).to.equal("1");
            expect(result.actualTotal.quote.value.toString()).to.equal("1");
            expect(result.total.base.value.toString()).to.equal("2");
            expect(result.total.quote.value.toString()).to.equal("1");
            expect(result.settled.base.value.toString()).to.equal("2");
            expect(result.settled.quote.value.toString()).to.equal("1");
            expect(result.unsettled.base.value.toString()).to.equal("0");
            expect(result.unsettled.quote.value.toString()).to.equal("0");
            expect(result.toString()).to.equal("« « 2 BTC », « 1 USDT » »");
        });

        it("fetchBalance() should only update quote when only quote specified", async () => {
            // eslint-disable-next-line no-undefined
            const bal = new balances.FixedBalanceFetcher(inner, undefined, new Big("20000"));

            const result = await bal.fetchBalance();
            expect(result.actualTotal.base.value.toString()).to.equal("1");
            expect(result.actualTotal.quote.value.toString()).to.equal("1");
            expect(result.total.base.value.toString()).to.equal("1");
            expect(result.total.quote.value.toString()).to.equal("20000");
            expect(result.settled.base.value.toString()).to.equal("1");
            expect(result.settled.quote.value.toString()).to.equal("20000");
            expect(result.unsettled.base.value.toString()).to.equal("0");
            expect(result.unsettled.quote.value.toString()).to.equal("0");
            expect(result.toString()).to.equal("« « 1 BTC », « 20000 USDT » »");
        });
    });

    describe("AddingBalanceFetcher", () => {
        const inner = fakes.balanceFetcher();

        it("Constructor should successfully construct with valid parameters", () => {
            const bal = new balances.AddingBalanceFetcher(inner, new Big("2"), new Big("7"));
            expect(bal.constructor.name).to.equal("AddingBalanceFetcher");
            expect(bal instanceof balances.AddingBalanceFetcher).to.be.true;
            expect(bal.isBalanceFetcher).to.be.true;
        });

        it("Constructor should fail with invalid inner balance fetcher", () => {
            const shouldThrow = () => new balances.AddingBalanceFetcher("invalid", new Big("2"), new Big("7"));
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid additional quote balance", () => {
            const shouldThrow = () => new balances.AddingBalanceFetcher(inner, "invalid", new Big("7"));
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid additional quote balance", () => {
            const shouldThrow = () => new balances.AddingBalanceFetcher(inner, new Big("2"), "invalid");
            expect(shouldThrow).to.throw();
        });

        it("fetchBalance() should return added balance values", async () => {
            const bal = new balances.AddingBalanceFetcher(inner, new Big("2"), new Big("7"));

            const result = await bal.fetchBalance();
            expect(result.actualTotal.base.value.toString()).to.equal("1");
            expect(result.actualTotal.quote.value.toString()).to.equal("1");
            expect(result.total.base.value.toString()).to.equal("3");
            expect(result.total.quote.value.toString()).to.equal("8");
            expect(result.settled.base.value.toString()).to.equal("3");
            expect(result.settled.quote.value.toString()).to.equal("8");
            expect(result.unsettled.base.value.toString()).to.equal("0");
            expect(result.unsettled.quote.value.toString()).to.equal("0");
            expect(result.toString()).to.equal("« « 3 BTC », « 8 USDT » »");
        });

        it("fetchBalance() should only update base when only base specified", async () => {
            const bal = new balances.AddingBalanceFetcher(inner, new Big("2"), numbers.Zero);

            const result = await bal.fetchBalance();
            expect(result.actualTotal.base.value.toString()).to.equal("1");
            expect(result.actualTotal.quote.value.toString()).to.equal("1");
            expect(result.total.base.value.toString()).to.equal("3");
            expect(result.total.quote.value.toString()).to.equal("1");
            expect(result.settled.base.value.toString()).to.equal("3");
            expect(result.settled.quote.value.toString()).to.equal("1");
            expect(result.unsettled.base.value.toString()).to.equal("0");
            expect(result.unsettled.quote.value.toString()).to.equal("0");
            expect(result.toString()).to.equal("« « 3 BTC », « 1 USDT » »");
        });

        it("fetchBalance() should only update quote when only quote specified", async () => {
            const bal = new balances.AddingBalanceFetcher(inner, numbers.Zero, new Big("7"));

            const result = await bal.fetchBalance();
            expect(result.actualTotal.base.value.toString()).to.equal("1");
            expect(result.actualTotal.quote.value.toString()).to.equal("1");
            expect(result.total.base.value.toString()).to.equal("1");
            expect(result.total.quote.value.toString()).to.equal("8");
            expect(result.settled.base.value.toString()).to.equal("1");
            expect(result.settled.quote.value.toString()).to.equal("8");
            expect(result.unsettled.base.value.toString()).to.equal("0");
            expect(result.unsettled.quote.value.toString()).to.equal("0");
            expect(result.toString()).to.equal("« « 1 BTC », « 8 USDT » »");
        });
    });

    describe("MultiplyingBalanceFetcher", () => {
        const inner = fakes.balanceFetcher();

        it("Constructor should successfully construct with valid parameters", () => {
            const bal = new balances.MultiplyingBalanceFetcher(inner, new Big("10"));
            expect(bal.constructor.name).to.equal("MultiplyingBalanceFetcher");
            expect(bal instanceof balances.MultiplyingBalanceFetcher).to.be.true;
            expect(bal.isBalanceFetcher).to.be.true;
        });

        it("Constructor should fail with invalid inner balance fetcher", () => {
            const shouldThrow = () => new balances.MultiplyingBalanceFetcher("invalid", new Big("10"));
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid adjustment factor", () => {
            const shouldThrow = () => new balances.MultiplyingBalanceFetcher(inner, "invalid");
            expect(shouldThrow).to.throw();
        });

        it("fetchBalance() should return added balance values", async () => {
            const bal = new balances.MultiplyingBalanceFetcher(inner, new Big("10"));

            const result = await bal.fetchBalance();
            expect(result.actualTotal.base.value.toString()).to.equal("1");
            expect(result.actualTotal.quote.value.toString()).to.equal("1");
            expect(result.total.base.value.toString()).to.equal("10");
            expect(result.total.quote.value.toString()).to.equal("10");
            expect(result.settled.base.value.toString()).to.equal("10");
            expect(result.settled.quote.value.toString()).to.equal("10");
            expect(result.unsettled.base.value.toString()).to.equal("0");
            expect(result.unsettled.quote.value.toString()).to.equal("0");
            expect(result.toString()).to.equal("« « 10 BTC », « 10 USDT » »");
        });
    });

    describe("SpreadMultiplyingBalanceFetcher", () => {
        const market = fakes.market();
        const inner = fakes.balanceFetcher();

        it("Constructor should successfully construct with valid parameters", () => {
            const bal = new balances.SpreadMultiplyingBalanceFetcher(inner, market, new Big("5"));
            expect(bal.constructor.name).to.equal("SpreadMultiplyingBalanceFetcher");
            expect(bal instanceof balances.SpreadMultiplyingBalanceFetcher).to.be.true;
            expect(bal.isBalanceFetcher).to.be.true;
        });

        it("Constructor should fail with invalid inner balance fetcher", () => {
            const shouldThrow = () => new balances.SpreadMultiplyingBalanceFetcher("invalid", market, new Big("5"));
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const shouldThrow = () => new balances.SpreadMultiplyingBalanceFetcher(inner, "invalid", new Big("5"));
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid adjustment factor", () => {
            const shouldThrow = () => new balances.SpreadMultiplyingBalanceFetcher(inner, market, "invalid");
            expect(shouldThrow).to.throw();
        });

        it("fetchBalance() should return balance values adjusted by spread factor 5", async () => {
            const bal = new balances.SpreadMultiplyingBalanceFetcher(inner, market, new Big("5"));

            // Fake spread is 99 / 101, so mid price is 100.
            // Base is then 5 (spreadAdjustmentFactor) + 1 (actual) = 6
            // Quote is then 500 (spreadAdjustmentFactor * mid price) + 1 (actual) = 501
            const result = await bal.fetchBalance();
            expect(result.actualTotal.base.value.toString()).to.equal("1");
            expect(result.actualTotal.quote.value.toString()).to.equal("1");
            expect(result.total.base.value.toString()).to.equal("6");
            expect(result.total.quote.value.toString()).to.equal("501");
            expect(result.settled.base.value.toString()).to.equal("6");
            expect(result.settled.quote.value.toString()).to.equal("501");
            expect(result.unsettled.base.value.toString()).to.equal("0");
            expect(result.unsettled.quote.value.toString()).to.equal("0");
            expect(result.toString()).to.equal("« « 6 BTC », « 501 USDT » »");
        });

        it("fetchBalance() should return balance values adjusted by spread factor 10", async () => {
            const bal = new balances.SpreadMultiplyingBalanceFetcher(inner, market, new Big("10"));

            // Fake spread is 99 / 101, so mid price is 100.
            // Base is then 10 (spreadAdjustmentFactor) + 1 (actual) = 11
            // Quote is then 1000 (spreadAdjustmentFactor * mid price) + 1 (actual) = 1001
            const result = await bal.fetchBalance();
            expect(result.actualTotal.base.value.toString()).to.equal("1");
            expect(result.actualTotal.quote.value.toString()).to.equal("1");
            expect(result.total.base.value.toString()).to.equal("11");
            expect(result.total.quote.value.toString()).to.equal("1001");
            expect(result.settled.base.value.toString()).to.equal("11");
            expect(result.settled.quote.value.toString()).to.equal("1001");
            expect(result.unsettled.base.value.toString()).to.equal("0");
            expect(result.unsettled.quote.value.toString()).to.equal("0");
            expect(result.toString()).to.equal("« « 11 BTC », « 1001 USDT » »");
        });
    });
});