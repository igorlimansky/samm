"use strict";

const Big = require("big.js");
const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const ordertracker = require("../lib/ordertracker.js");

describe("OrderTracker", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    it("Constructor should succeed with no parameters", () => {
        const orderTracker = new ordertracker.OrderTracker();
        expect(orderTracker.constructor.name).to.equal("OrderTracker");
        expect(orderTracker._counter._counter).to.equal(0);
    });

    it("Constructor should succeed with startAt parameters", () => {
        const orderTracker = new ordertracker.OrderTracker(27);
        expect(orderTracker.constructor.name).to.equal("OrderTracker");
        expect(orderTracker._counter._counter).to.equal(27);
    });

    it("randomStart() should succeed constructing valid OrderTracker", () => {
        const orderTracker = ordertracker.OrderTracker.randomStartAt();
        expect(orderTracker.constructor.name).to.equal("OrderTracker");
    });

    it("randomStart() startAt should be aligned to 10,000", () => {
        const orderTracker = ordertracker.OrderTracker.randomStartAt();
        expect(orderTracker.constructor.name).to.equal("OrderTracker");
        expect(orderTracker._counter._counter % 10000).to.equal(0);
    });

    it("trackOrders() should store orders", () => {
        const orderTracker = new ordertracker.OrderTracker();
        orderTracker.trackOrders(fakes.orders(101, 102));
        expect(orderTracker._orders.length).to.equal(2);
        expect(orderTracker._orders[0].clientId.eq(new Big(101))).to.be.true;
        expect(orderTracker._orders[1].clientId.eq(new Big(102))).to.be.true;
    });

    it("clearTrackedOrders() should succeed on fresh object", () => {
        const orderTracker = new ordertracker.OrderTracker();
        orderTracker.clearTrackedOrders();
    });

    it("clearTrackedOrders() should clear existing orders", () => {
        const orderTracker = new ordertracker.OrderTracker();
        orderTracker.trackOrders(fakes.orders(101, 102));
        expect(orderTracker._orders.length).to.equal(2);
        orderTracker.clearTrackedOrders();
        expect(orderTracker._orders.length).to.equal(0);
    });

    it("allIdsTracked() should return true for all tracked client IDs", () => {
        const orderTracker = new ordertracker.OrderTracker();
        const fakeOrders = fakes.orders(101, 102);

        orderTracker.trackOrders(fakeOrders);
        expect(orderTracker.allIdsTracked([fakeOrders[0].clientId, fakeOrders[1].clientId])).to.be.true;
    });

    it("allIdsTracked() should return false if missing a client ID", () => {
        const orderTracker = new ordertracker.OrderTracker();
        const fakeOrders = fakes.orders(101, 102);

        orderTracker.trackOrders(fakeOrders);
        expect(orderTracker.allIdsTracked([fakeOrders[0].clientId])).to.be.false;
    });

    it("allIdsTracked() should return false if given an extra client ID", () => {
        const orderTracker = new ordertracker.OrderTracker();
        const fakeOrders = fakes.orders(101, 102);

        orderTracker.trackOrders(fakeOrders);
        expect(orderTracker.allIdsTracked(
            [
                fakeOrders[0].clientId,
                fakeOrders[1].clientId,
                new Big(103)
            ]
        )).to.be.false;
    });

    it("getOrderByClientId() should return the correct order", () => {
        const orderTracker = new ordertracker.OrderTracker();
        const fakeOrders = fakes.orders(101, 102);
        orderTracker.trackOrders(fakeOrders);
        const first = orderTracker.getOrderByClientId(fakeOrders[0].clientId);
        const second = orderTracker.getOrderByClientId(fakeOrders[1].clientId);
        expect(first).to.equal(fakeOrders[0]);
        expect(second).to.equal(fakeOrders[1]);
    });

    it("getOrderByClientId() should return null if client ID not tracked", () => {
        const orderTracker = new ordertracker.OrderTracker();
        orderTracker.trackOrders(fakes.orders(101, 102));
        const nonExistent = orderTracker.getOrderByClientId(new Big(103));
        expect(nonExistent).to.equal(null);
    });

    it("nextClientId() should return a sequence of increasing IDs", () => {
        const orderTracker = new ordertracker.OrderTracker();
        for (let counter = 1; counter <= 10; counter += 1) {
            const clientId = orderTracker.nextClientId();
            expect(clientId.eq(new Big(counter))).to.be.true;
        }
    });

    it("nextClientId() should overflow properly", () => {
        const orderTracker = new ordertracker.OrderTracker(Number.MAX_SAFE_INTEGER);
        const overflowed = orderTracker.nextClientId();
        expect(Number(overflowed)).to.equal(1);
    });

    it("nextClientId() series should overflow properly", () => {
        const orderTracker = new ordertracker.OrderTracker(Number.MAX_SAFE_INTEGER - 1);
        const max = orderTracker.nextClientId();
        const overflowed = orderTracker.nextClientId();
        const next = orderTracker.nextClientId();
        expect(Number(max)).to.equal(Number.MAX_SAFE_INTEGER);
        expect(Number(overflowed)).to.equal(1);
        expect(Number(next)).to.equal(2);
    });

    it("_isTrackedClientId() should return true for all tracked client IDs", () => {
        const orderTracker = new ordertracker.OrderTracker();
        const orders = fakes.orders(101, 102);
        orderTracker.trackOrders(orders);
        expect(orderTracker._orders.length).to.equal(2);
        expect(orderTracker._isTrackedClientId(orders[0].clientId)).to.be.true;
        expect(orderTracker._isTrackedClientId(orders[1].clientId)).to.be.true;
    });

    it("_isTrackedClientId() should return false for unseen client ID", () => {
        const orderTracker = new ordertracker.OrderTracker();
        orderTracker.trackOrders(fakes.orders(101, 102));
        expect(orderTracker._orders.length).to.equal(2);
        expect(orderTracker._isTrackedClientId(new Big(7))).to.be.false;
    });
});