"use strict";

const accounttracker = require("../lib/accounttracker.js");
const {
    expect
} = require("chai");
const fillreporter = require("../lib/fillreporter.js");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const ordertracker = require("../lib/ordertracker.js");

describe("FillReporter", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    describe("AuditingFillReporter", () => {
        it("Constructor should fail with no parameters", () => {
            const shouldThrow = () => new fillreporter.AuditingFillReporter();
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid context", () => {
            const shouldThrow = () => new fillreporter.AuditingFillReporter(
                {},
                fakes.market(),
                fakes.auditor(),
                new accounttracker.SimulationAccountTracker(fakes.tokenAccount(), fakes.tokenAccount(), true),
                new ordertracker.OrderTracker()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const shouldThrow = () => new fillreporter.AuditingFillReporter(
                fakes.context(),
                {},
                fakes.auditor(),
                new accounttracker.SimulationAccountTracker(fakes.tokenAccount(), fakes.tokenAccount(), true),
                new ordertracker.OrderTracker()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid auditor", () => {
            const shouldThrow = () => new fillreporter.AuditingFillReporter(
                fakes.context(),
                fakes.market(),
                {},
                new accounttracker.SimulationAccountTracker(fakes.tokenAccount(), fakes.tokenAccount(), true),
                new ordertracker.OrderTracker()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid account tracker", () => {
            const shouldThrow = () => new fillreporter.AuditingFillReporter(
                fakes.context(),
                fakes.market(),
                fakes.auditor(),
                {},
                new ordertracker.OrderTracker()
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid order tracker", () => {
            const shouldThrow = () => new fillreporter.AuditingFillReporter(
                fakes.context(),
                fakes.market(),
                fakes.auditor(),
                new accounttracker.SimulationAccountTracker([fakes.tokenAccount()], [fakes.tokenAccount()], true),
                {}
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should succeed with valid parameters", () => {
            const fillReporter = new fillreporter.AuditingFillReporter(
                fakes.context(),
                fakes.market(),
                fakes.auditor(),
                new accounttracker.SimulationAccountTracker([fakes.tokenAccount()], [fakes.tokenAccount()], true),
                new ordertracker.OrderTracker()
            );
            expect(fillReporter.constructor.name).to.equal("AuditingFillReporter");
        });

        it("reportOnFills() should call auditor.fill() twice for two fills.", async () => {
            const auditor = fakes.auditor();
            const fillReporter = new fillreporter.AuditingFillReporter(
                fakes.context(),
                fakes.market(),
                auditor,
                new accounttracker.SimulationAccountTracker([fakes.tokenAccount()], [fakes.tokenAccount()], true),
                new ordertracker.OrderTracker()
            );
            await fillReporter.reportOnFills();
            expect(auditor.fill.called).to.be.true;
            expect(auditor.fill.calledTwice).to.be.true;
        });

        it("reportOnFills() should only call auditor.fill() twice for two fills, even when re-sent.", async () => {
            const auditor = fakes.auditor();
            const fillReporter = new fillreporter.AuditingFillReporter(
                fakes.context(),
                fakes.market(),
                auditor,
                new accounttracker.SimulationAccountTracker([fakes.tokenAccount()], [fakes.tokenAccount()], true),
                new ordertracker.OrderTracker()
            );
            await fillReporter.reportOnFills();
            await fillReporter.reportOnFills();
            expect(auditor.fill.called).to.be.true;
            expect(auditor.fill.calledTwice).to.be.true;
        });
    });
});