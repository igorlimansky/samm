"use strict";

const {
    expect
} = require("chai");
const log = require("../lib/log.js");
const tags = require("../lib/tags.js");

const TEST_TAG_UPPERCASE = "ABCDEF";
const TEST_TAG_LOWERCASE = "abcdef";
const COMPOUND_TAG = "PartA:PartB:PartC";
const ALT_TAG = "SAMM-TEST";
const NEVER_TAG = "NEVER";

describe("Tags", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    describe("Tag", () => {
        it("Constructor should successfully construct with valid parameters", () => {
            const actual = new tags.Tag(TEST_TAG_LOWERCASE);

            expect(actual.constructor.name).to.equal("Tag");

            expect(actual.name).to.equal(TEST_TAG_UPPERCASE);
            expect(actual.toString()).to.equal(`« ${TEST_TAG_UPPERCASE} »`);
            expect(actual.isCompound).to.be.false;
            expect(actual.parts).to.be.an("array")
                .to.have.lengthOf(1)
                .that.has.members([TEST_TAG_UPPERCASE])
                .and.not.members([NEVER_TAG]);
        });

        it("Constructor should throw with invalid order side", () => {
            const shouldThrow = () => new tags.Tag({});
            expect(shouldThrow).to.throw();
        });

        it("name property should be normalised", () => {
            const actual = new tags.Tag(TEST_TAG_LOWERCASE);
            expect(actual.name).to.equal(TEST_TAG_UPPERCASE);
        });

        it("isCompound property should be false for non-compound tag", () => {
            const actual = new tags.Tag(TEST_TAG_LOWERCASE);
            expect(actual.isCompound).to.be.false;
        });

        it("isCompound property should be true for compound tag", () => {
            const actual = new tags.Tag(COMPOUND_TAG);
            expect(actual.isCompound).to.be.true;
        });

        it("parts property should have single element for non-compound tag", () => {
            const actual = new tags.Tag(TEST_TAG_LOWERCASE);
            expect(actual.parts).to.be.an("array")
                .to.have.lengthOf(1)
                .that.has.members([TEST_TAG_UPPERCASE])
                .and.not.members([NEVER_TAG]);
        });

        it("parts property should have all elements for compound tag", () => {
            const actual = new tags.Tag(COMPOUND_TAG);
            expect(actual.parts).to.be.an("array")
                .to.have.lengthOf(3)
                .that.has.members(["PARTA", "PARTB", "PARTC"])
                .and.not.members([NEVER_TAG]);
        });

        it("is() should be true for normalised tag name", () => {
            const actual = new tags.Tag(TEST_TAG_LOWERCASE);
            expect(actual.is(TEST_TAG_UPPERCASE)).to.be.true;
        });

        it("is() should be true for non-normalised tag name", () => {
            const actual = new tags.Tag(TEST_TAG_LOWERCASE);
            expect(actual.is(TEST_TAG_LOWERCASE)).to.be.true;
        });

        it("is() should be false for nonexistent tag name", () => {
            const actual = new tags.Tag(TEST_TAG_LOWERCASE);
            expect(actual.is(NEVER_TAG)).to.be.false;
        });

        it("hasPrefix() should be true for entirety of non-compound tag", () => {
            const actual = new tags.Tag(TEST_TAG_UPPERCASE);
            expect(actual.hasPrefix(TEST_TAG_UPPERCASE)).to.be.true;
        });

        it("hasPrefix() should be true for entirety of non-normalised non-compound tag", () => {
            const actual = new tags.Tag(TEST_TAG_UPPERCASE);
            expect(actual.hasPrefix(TEST_TAG_LOWERCASE)).to.be.true;
        });

        it("hasPrefix() should be false for entirety of non-compound tag", () => {
            const actual = new tags.Tag(TEST_TAG_UPPERCASE);
            expect(actual.hasPrefix(NEVER_TAG)).to.be.false;
        });

        it("hasPrefix() should be true for prefix of compound tag", () => {
            const actual = new tags.Tag(COMPOUND_TAG);
            expect(actual.hasPrefix("PARTA")).to.be.true;
        });

        it("hasPrefix() should be true for non-normalised prefix of compound tag", () => {
            const actual = new tags.Tag(COMPOUND_TAG);
            expect(actual.hasPrefix("PartA")).to.be.true;
        });

        it("hasPrefix() should be false for non-prefix of compound tag", () => {
            const actual = new tags.Tag(COMPOUND_TAG);
            expect(actual.hasPrefix("NOTPREFIX")).to.be.false;
        });

        it("hasPrefix() should be false for second part of compound tag", () => {
            const actual = new tags.Tag(COMPOUND_TAG);
            expect(actual.hasPrefix("PARTB")).to.be.false;
        });

        it("equals() should be true for self", () => {
            const actual = new tags.Tag(COMPOUND_TAG);
            expect(actual.equals(actual)).to.be.true;
        });

        it("equals() should be true for tag with same name", () => {
            const actual = new tags.Tag(TEST_TAG_UPPERCASE);
            const compared = new tags.Tag(TEST_TAG_UPPERCASE);
            expect(actual.equals(compared)).to.be.true;
        });

        it("equals() should be true for tag with same normalised name", () => {
            const actual = new tags.Tag(TEST_TAG_UPPERCASE);
            const compared = new tags.Tag(TEST_TAG_LOWERCASE);
            expect(actual.equals(compared)).to.be.true;
        });

        it("equals() should be false for tag with different name", () => {
            const actual = new tags.Tag(TEST_TAG_UPPERCASE);
            const compared = new tags.Tag(ALT_TAG);
            expect(actual.equals(compared)).to.be.false;
        });

        it("equals() should throw with invalid tag parameter", () => {
            const actual = new tags.Tag(TEST_TAG_UPPERCASE);
            const shouldThrow = () => actual.equals({});
            expect(shouldThrow).to.throw();
        });

        it("toString() should render tag properly", () => {
            const actual = new tags.Tag(TEST_TAG_UPPERCASE);
            expect(actual.toString()).to.equal(`« ${TEST_TAG_UPPERCASE} »`);
        });
    });

    describe("TagCollection", () => {
        function buildTagged() {
            const tagCollection = new tags.TagCollection();
            tagCollection.add("A", "B", "C");
            return tagCollection;
        }

        it("has() should return false when no parameter", () => {
            const actual = new tags.TagCollection();
            expect(actual.has()).to.be.false;
        });

        it("has() should return false when no parameter", () => {
            const actual = buildTagged();
            expect(actual).to.have.lengthOf(3);
            const empty = [];
            expect(actual.has(...empty)).to.be.false;
            expect(actual).to.have.lengthOf(3);
        });

        it("has() should fail if passed an empty array", () => {
            const actual = buildTagged();
            expect(actual).to.have.lengthOf(3);
            const empty = [];
            const shouldThrow = () => actual.has(empty);
            expect(shouldThrow).to.throw();
        });

        it("has() should have first tag", () => {
            const actual = buildTagged();
            expect(actual.has("A")).to.be.true;
        });

        it("has() should have first tag (case-insensitive)", () => {
            const actual = buildTagged();
            expect(actual.has("a")).to.be.true;
        });

        it("has() should have last tag", () => {
            const actual = buildTagged();
            expect(actual.has("C")).to.be.true;
        });

        it("has() should have last tag (case-insensitive)", () => {
            const actual = buildTagged();
            expect(actual.has("c")).to.be.true;
        });

        it("has() should have non-first, non-last tag", () => {
            const actual = buildTagged();
            expect(actual.has("B")).to.be.true;
        });

        it("has() should have non-first, non-last tag (case-insensitive)", () => {
            const actual = buildTagged();
            expect(actual.has("b")).to.be.true;
        });

        it("has() should not have nonexistent tag", () => {
            const actual = buildTagged();
            expect(actual.has("D")).to.be.false;
        });

        it("has() should not have nonexistent tag (case-insensitive)", () => {
            const actual = buildTagged();
            expect(actual.has("d")).to.be.false;
        });

        it("has() should have all tags in call", () => {
            const actual = buildTagged();
            expect(actual.has("B", "C")).to.be.true;
        });

        it("has() should check all tags in call", () => {
            const actual = buildTagged();
            expect(actual.has("B", "C", "D")).to.be.false;
        });

        it("remove() should remove no tags if passed an empty destructured array", () => {
            const actual = buildTagged();
            expect(actual).to.have.lengthOf(3);
            const empty = [];
            actual.remove(...empty);
            expect(actual).to.have.lengthOf(3);
        });

        it("remove() should fail if passed an empty array", () => {
            const actual = buildTagged();
            expect(actual).to.have.lengthOf(3);
            const empty = [];
            const shouldThrow = () => actual.remove(empty);
            expect(shouldThrow).to.throw();
        });

        it("remove() should remove first tag", () => {
            const actual = buildTagged();
            expect(actual).to.have.lengthOf(3);
            expect(actual.has("A")).to.be.true;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.true;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.true;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.true;
            expect(actual.has("d")).to.be.false;
            actual.remove("A");
            expect(actual.has("A")).to.be.false;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.true;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.false;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.true;
            expect(actual.has("d")).to.be.false;
            expect(actual).to.have.lengthOf(2);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members(["B", "C"])
                .and.not.members([NEVER_TAG]);
        });

        it("remove() should remove first tag (case-insensitive)", () => {
            const actual = buildTagged();
            expect(actual).to.have.lengthOf(3);
            expect(actual.has("A")).to.be.true;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.true;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.true;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.true;
            expect(actual.has("d")).to.be.false;
            actual.remove("a");
            expect(actual.has("A")).to.be.false;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.true;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.false;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.true;
            expect(actual.has("d")).to.be.false;
            expect(actual).to.have.lengthOf(2);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members(["B", "C"])
                .and.not.members([NEVER_TAG]);
        });

        it("remove() should remove last tag", () => {
            const actual = buildTagged();
            expect(actual).to.have.lengthOf(3);
            expect(actual.has("A")).to.be.true;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.true;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.true;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.true;
            expect(actual.has("d")).to.be.false;
            actual.remove("C");
            expect(actual.has("A")).to.be.true;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.false;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.true;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.false;
            expect(actual.has("d")).to.be.false;
            expect(actual).to.have.lengthOf(2);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members(["A", "B"])
                .and.not.members([NEVER_TAG]);
        });

        it("remove() should remove last tag (case-insensitive)", () => {
            const actual = buildTagged();
            expect(actual).to.have.lengthOf(3);
            expect(actual.has("A")).to.be.true;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.true;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.true;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.true;
            expect(actual.has("d")).to.be.false;
            actual.remove("c");
            expect(actual.has("A")).to.be.true;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.false;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.true;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.false;
            expect(actual.has("d")).to.be.false;
            expect(actual).to.have.lengthOf(2);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members(["A", "B"])
                .and.not.members([NEVER_TAG]);
        });

        it("remove() should remove non-first, non-last tag", () => {
            const actual = buildTagged();
            expect(actual).to.have.lengthOf(3);
            expect(actual.has("A")).to.be.true;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.true;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.true;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.true;
            expect(actual.has("d")).to.be.false;
            actual.remove("B");
            expect(actual.has("A")).to.be.true;
            expect(actual.has("B")).to.be.false;
            expect(actual.has("C")).to.be.true;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.true;
            expect(actual.has("b")).to.be.false;
            expect(actual.has("c")).to.be.true;
            expect(actual.has("d")).to.be.false;
            expect(actual).to.have.lengthOf(2);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members(["A", "C"])
                .and.not.members([NEVER_TAG]);
        });

        it("remove() should remove non-first, non-last tag (case-insensitive)", () => {
            const actual = buildTagged();
            expect(actual).to.have.lengthOf(3);
            expect(actual.has("A")).to.be.true;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.true;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.true;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.true;
            expect(actual.has("d")).to.be.false;
            actual.remove("b");
            expect(actual.has("A")).to.be.true;
            expect(actual.has("B")).to.be.false;
            expect(actual.has("C")).to.be.true;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.true;
            expect(actual.has("b")).to.be.false;
            expect(actual.has("c")).to.be.true;
            expect(actual.has("d")).to.be.false;
            expect(actual).to.have.lengthOf(2);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members(["A", "C"])
                .and.not.members([NEVER_TAG]);
        });

        it("remove() should remove first and last tags in one call", () => {
            const actual = buildTagged();
            expect(actual).to.have.lengthOf(3);
            expect(actual.has("A")).to.be.true;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.true;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.true;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.true;
            expect(actual.has("d")).to.be.false;
            actual.remove("A", "C");
            expect(actual.has("A")).to.be.false;
            expect(actual.has("B")).to.be.true;
            expect(actual.has("C")).to.be.false;
            expect(actual.has("D")).to.be.false;
            expect(actual.has("a")).to.be.false;
            expect(actual.has("b")).to.be.true;
            expect(actual.has("c")).to.be.false;
            expect(actual.has("d")).to.be.false;
            expect(actual).to.have.lengthOf(1);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members(["B"])
                .and.not.members([NEVER_TAG, "C"]);
        });

        it("add() should add no tags if passed an empty destructured array", () => {
            const actual = new tags.TagCollection();
            expect(actual).to.have.lengthOf(0);
            const empty = [];
            actual.add(...empty);
            expect(actual).to.have.lengthOf(0);
        });

        it("add() should fail if passed an empty array", () => {
            const actual = new tags.TagCollection();
            expect(actual).to.have.lengthOf(0);
            const empty = [];
            const shouldThrow = () => actual.add(empty);
            expect(shouldThrow).to.throw();
        });

        it("add() should add tag when empty tags exist", () => {
            const actual = new tags.TagCollection();
            expect(actual).to.have.lengthOf(0);
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.false;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.false;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual).to.have.lengthOf(1);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE])
                .and.not.members([NEVER_TAG]);
        });

        it("add() should add multiple tags in one go", () => {
            const actual = new tags.TagCollection();
            expect(actual).to.have.lengthOf(0);
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.false;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.false;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_UPPERCASE, ALT_TAG);
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual).to.have.lengthOf(2);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE, ALT_TAG])
                .and.not.members([NEVER_TAG]);
        });

        it("add() should add tag when tags exist", () => {
            const actual = new tags.TagCollection();
            actual.add(ALT_TAG);
            expect(actual).to.have.lengthOf(1);
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.false;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.false;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual).to.have.lengthOf(2);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE, ALT_TAG])
                .and.not.members([NEVER_TAG]);
        });

        it("add() should not add tag when already present", () => {
            const actual = new tags.TagCollection();
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual).to.have.lengthOf(1);
            expect(actual.has(ALT_TAG)).to.be.false;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual).to.have.lengthOf(1);
            expect(actual.has(ALT_TAG)).to.be.false;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE])
                .and.not.members([NEVER_TAG]);
        });

        it("add() should not add tag when already present and other tags are present", () => {
            const actual = new tags.TagCollection();
            actual.add(ALT_TAG);
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE, ALT_TAG])
                .and.not.members([NEVER_TAG]);
        });

        it("add() should not add tag when already present and other tags are present (reversed)", () => {
            const actual = new tags.TagCollection();
            actual.add(TEST_TAG_UPPERCASE);
            actual.add(ALT_TAG);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE, ALT_TAG])
                .and.not.members([NEVER_TAG]);
        });

        it("add() should not add tag when already present but differently cased", () => {
            const actual = new tags.TagCollection();
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual).to.have.lengthOf(1);
            expect(actual.has(ALT_TAG)).to.be.false;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_LOWERCASE);
            expect(actual).to.have.lengthOf(1);
            expect(actual.has(ALT_TAG)).to.be.false;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE])
                .and.not.members([NEVER_TAG]);
        });

        it("add() should not add tag when already present but differently cased and other tags are present", () => {
            const actual = new tags.TagCollection();
            actual.add(TEST_TAG_UPPERCASE);
            actual.add(ALT_TAG);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_LOWERCASE);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE, ALT_TAG])
                .and.not.members([NEVER_TAG]);
        });

        it("add() should not add tag when already present but differently cased and other tags are present (2)", () => {
            const actual = new tags.TagCollection();
            actual.add(ALT_TAG);
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_LOWERCASE);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE, ALT_TAG])
                .and.not.members([NEVER_TAG]);
        });

        it("add() should not add tag when already present but differently cased (2)", () => {
            const actual = new tags.TagCollection();
            actual.add(TEST_TAG_LOWERCASE);
            expect(actual).to.have.lengthOf(1);
            expect(actual.has(ALT_TAG)).to.be.false;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual).to.have.lengthOf(1);
            expect(actual.has(ALT_TAG)).to.be.false;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE])
                .and.not.members([NEVER_TAG]);
        });

        it("add() should not add tag when already present but differently cased (2) and other tags are present", () => {
            const actual = new tags.TagCollection();
            actual.add(ALT_TAG);
            actual.add(TEST_TAG_LOWERCASE);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE, ALT_TAG])
                .and.not.members([NEVER_TAG]);
        });

        it("add() should not add tag when present but differently cased (2) alongside other tags (reversed)", () => {
            const actual = new tags.TagCollection();
            actual.add(TEST_TAG_LOWERCASE);
            actual.add(ALT_TAG);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            actual.add(TEST_TAG_UPPERCASE);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has(ALT_TAG)).to.be.true;
            expect(actual.has(TEST_TAG_UPPERCASE)).to.be.true;
            expect(actual.has(TEST_TAG_LOWERCASE)).to.be.true;
            expect(actual.has(NEVER_TAG)).to.be.false;
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE, ALT_TAG])
                .and.not.members([NEVER_TAG]);
        });

        it("addFlags() should add specified flag values", () => {
            const flags = {
                value1: true,
                value2: true
            };
            const actual = new tags.TagCollection();
            actual.addFlags(flags);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has("flag:value1")).to.be.true;
            expect(actual.has("flag:value2")).to.be.true;
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members(["FLAG:VALUE1", "FLAG:VALUE2"])
                .and.not.members([NEVER_TAG]);
        });

        it("addFlags() should not add false flag values", () => {
            const flags = {
                value1: true,
                value2: false,
                value3: true
            };
            const actual = new tags.TagCollection();
            actual.addFlags(flags);
            expect(actual).to.have.lengthOf(2);
            expect(actual.has("flag:value1")).to.be.true;
            expect(actual.has("flag:value2")).to.be.false;
            expect(actual.has("flag:value3")).to.be.true;
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members(["FLAG:VALUE1", "FLAG:VALUE3"])
                .and.not.members([NEVER_TAG, "FLAG:VALUE2"]);
        });

        it("addFlags() should add compound values", () => {
            const flags = {
                value1: true,
                value2: true
            };
            const actual = new tags.TagCollection();
            actual.addFlags(flags);
            expect(actual).to.have.lengthOf(2);
            for (const tag of actual) {
                expect(tag.isCompound).to.be.true;
            }
        });

        it("addFlags() should add values prefixed with 'flag'", () => {
            const flags = {
                value1: true,
                value2: true
            };
            const actual = new tags.TagCollection();
            actual.addFlags(flags);
            expect(actual).to.have.lengthOf(2);
            for (const tag of actual) {
                expect(tag.hasPrefix("flag")).to.be.true;
            }
        });

        it("addFlags() should not overwrite existing values", () => {
            const flags = {
                value1: true,
                value2: true
            };
            const actual = new tags.TagCollection();
            actual.add(TEST_TAG_LOWERCASE);
            actual.add(ALT_TAG);
            actual.addFlags(flags);
            expect(actual).to.have.lengthOf(4);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE, ALT_TAG, "FLAG:VALUE1", "FLAG:VALUE2"])
                .and.not.members([NEVER_TAG]);
        });

        it("addFlags() should not prevent addition of new values", () => {
            const flags = {
                value1: true,
                value2: true
            };
            const actual = new tags.TagCollection();
            actual.addFlags(flags);
            actual.add(TEST_TAG_LOWERCASE);
            actual.add(ALT_TAG);
            expect(actual).to.have.lengthOf(4);
            expect(actual.allTagNames()).to.be.an("array")
                .that.has.members([TEST_TAG_UPPERCASE, ALT_TAG, "FLAG:VALUE1", "FLAG:VALUE2"])
                .and.not.members([NEVER_TAG]);
        });
    });

    describe("ensureTags", () => {
        it("ensureTags() should add basic TagCollection to default property of bare object", () => {
            const actual = {};
            expect(actual.tags).to.be.undefined;
            tags.ensureTags(actual);
            expect(actual.tags).to.be.an.instanceof(tags.TagCollection);
            expect(actual.tags).to.have.lengthOf(1);
            const [first] = actual.tags;
            expect(first.hasPrefix("timestamp")).to.be.true;
        });

        it("ensureTags() should add basic TagCollection to default property of named object", () => {
            const actual = {
                [Symbol.toStringTag]: "sometype"
            };
            expect(actual.tags).to.be.undefined;
            tags.ensureTags(actual);
            expect(actual.tags).to.be.an.instanceof(tags.TagCollection);
            expect(actual.tags).to.have.lengthOf(2);
            const [first, second] = actual.tags;
            expect(first.hasPrefix("type")).to.be.true;
            expect(first.name).to.equal("TYPE:SOMETYPE");
            expect(second.hasPrefix("timestamp")).to.be.true;
        });

        it("ensureTags() should add basic TagCollection to specified property of bare object", () => {
            const actual = {};
            expect(actual.someProperty).to.be.undefined;
            tags.ensureTags(actual, "someProperty");
            expect(actual.someProperty).to.be.an.instanceof(tags.TagCollection);
            expect(actual.someProperty).to.have.lengthOf(1);
            const [first] = actual.someProperty;
            expect(first.hasPrefix("timestamp")).to.be.true;
        });

        it("ensureTags() should add basic TagCollection to specified property of named object", () => {
            const actual = {
                [Symbol.toStringTag]: "sometype"
            };
            expect(actual.someProperty).to.be.undefined;
            tags.ensureTags(actual, "someProperty");
            expect(actual.someProperty).to.be.an.instanceof(tags.TagCollection);
            expect(actual.someProperty).to.have.lengthOf(2);
            const [first, second] = actual.someProperty;
            expect(first.hasPrefix("type")).to.be.true;
            expect(first.name).to.equal("TYPE:SOMETYPE");
            expect(second.hasPrefix("timestamp")).to.be.true;
        });

        it("ensureTags() should not overwrite existing tags", () => {
            const actual = {tags: new tags.TagCollection()};
            expect(actual.tags).to.be.an.instanceof(tags.TagCollection);
            actual.tags.add("A", "B", "C");
            expect(actual.tags.has("A")).to.be.true;
            expect(actual.tags.has("B")).to.be.true;
            expect(actual.tags.has("C")).to.be.true;

            tags.ensureTags(actual);

            expect(actual.tags).to.be.an.instanceof(tags.TagCollection);
            expect(actual.tags).to.have.lengthOf(3);
            expect(actual.tags.has("A")).to.be.true;
            expect(actual.tags.has("B")).to.be.true;
            expect(actual.tags.has("C")).to.be.true;
            expect(actual.tags.allTagNames()).to.be.an("array")
                .that.has.members(["A", "B", "C"])
                .and.not.members([NEVER_TAG]);
        });


        it("ensureTags() should not set default property if different property specified", () => {
            const actual = {};
            expect(actual.tags).to.be.undefined;
            tags.ensureTags(actual, "someProperty");
            expect(actual.tags).to.be.undefined;
        });

        it("ensureTags() should not alter default property if different property specified", () => {
            const actual = {tags: 17};
            expect(actual.tags).to.equal(17);
            tags.ensureTags(actual, "someProperty");
            expect(actual.tags).to.equal(17);
        });

        it("ensureTags() should throw if it would overwrite an already-set default property", () => {
            const actual = {tags: 17};
            expect(actual.tags).to.equal(17);
            const shouldThrow = () => tags.ensureTags(actual);
            expect(shouldThrow).to.throw();
        });

        it("ensureTags() should throw if it would overwrite an already-set specified property", () => {
            const actual = {otherProperty: 17};
            expect(actual.tags).to.be.undefined;
            expect(actual.otherProperty).to.equal(17);
            const shouldThrow = () => tags.ensureTags(actual, "otherProperty");
            expect(shouldThrow).to.throw();
        });
    });
});