"use strict";

const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const orderfetcher = require("../lib/orderfetcher.js");
const ordertracker = require("../lib/ordertracker.js");
const orderwaiter = require("../lib/orderwaiter.js");

describe("OrderWaiter", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    describe("SimulationOrderWaiter", () => {
        it("Constructor should succeed with no parameters", () => {
            const orderWaiter = new orderwaiter.SimulationOrderWaiter();
            expect(orderWaiter.constructor.name).to.equal("SimulationOrderWaiter");
        });

        it("waitForOrders() should return immediately", async () => {
            const orderWaiter = new orderwaiter.SimulationOrderWaiter();
            await orderWaiter.waitForOrders();
        });
    });

    describe("SyncOrderWaiter", () => {
        it("Constructor should succeed with valid parameters", () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderFetcher = new orderfetcher.SimulationOrderFetcher();
            const orderTracker = new ordertracker.OrderTracker();
            const orderWaiter = new orderwaiter.SyncOrderWaiter(
                context,
                market,
                orderFetcher,
                orderTracker,
                2,
                60
            );
            expect(orderWaiter.constructor.name).to.equal("SyncOrderWaiter");
        });

        it("Constructor should fail with invalid context", () => {
            const market = fakes.market();
            const orderFetcher = new orderfetcher.SimulationOrderFetcher();
            const orderTracker = new ordertracker.OrderTracker();
            const shouldThrow = () => new orderwaiter.SyncOrderWaiter(
                "invalid",
                market,
                orderFetcher,
                orderTracker,
                3,
                120
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid market", () => {
            const context = fakes.context();
            const orderFetcher = new orderfetcher.SimulationOrderFetcher();
            const orderTracker = new ordertracker.OrderTracker();
            const shouldThrow = () => new orderwaiter.SyncOrderWaiter(
                context,
                "invalid",
                orderFetcher,
                orderTracker,
                3,
                120
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid order fetcher", () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderTracker = new ordertracker.OrderTracker();
            const shouldThrow = () => new orderwaiter.SyncOrderWaiter(
                context,
                market,
                "invalid",
                orderTracker,
                3,
                120
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid order tracker", () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderFetcher = new orderfetcher.SimulationOrderFetcher();
            const shouldThrow = () => new orderwaiter.SyncOrderWaiter(
                context,
                market,
                orderFetcher,
                "invalid",
                3,
                120
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid sleep increment", () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderFetcher = new orderfetcher.SimulationOrderFetcher();
            const orderTracker = new ordertracker.OrderTracker();
            const shouldThrow = () => new orderwaiter.SyncOrderWaiter(
                context,
                market,
                orderFetcher,
                orderTracker,
                "invalid",
                300
            );
            expect(shouldThrow).to.throw();
        });

        it("Constructor should fail with invalid timeoout", () => {
            const context = fakes.context();
            const market = fakes.market();
            const orderFetcher = new orderfetcher.SimulationOrderFetcher();
            const orderTracker = new ordertracker.OrderTracker();
            const shouldThrow = () => new orderwaiter.SyncOrderWaiter(
                context,
                market,
                orderFetcher,
                orderTracker,
                5,
                "invalid"
            );
            expect(shouldThrow).to.throw();
        });
    });
});