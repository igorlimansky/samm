"use strict";

const accounttracker = require("../lib/accounttracker.js");
const audit = require("../lib/auditor.js");
const balances = require("../lib/balances.js");
const Big = require("big.js");
const ctx = require("../lib/context.js");
const fillreporter = require("../lib/fillreporter.js");
const mkts = require("../lib/markets.js");
const numbers = require("../lib/numbers.js");
const order = require("../lib/order.js");
const orderbuilder = require("../lib/orderbuilder.js");
const orderplacer = require("../lib/orderplacer.js");
const serum = require("@project-serum/serum");
const settler = require("../lib/settler.js");
const sinon = require("sinon");
const solana = require("@solana/web3.js");
const spreads = require("../lib/spreads.js");
const token = require("../lib/token.js");

const FAKE_DECIMAL_PLACES = 6;

function randomAccount() {
    return new solana.Account();
}

function randomPublicKey() {
    return new solana.Account().publicKey;
}

const FAKE_BASE_ADDRESS = randomAccount().publicKey;
const FAKE_QUOTE_ADDRESS = randomAccount().publicKey;
const FAKE_BASE_TOKEN = new token.Token("FAKEBASE", FAKE_BASE_ADDRESS, token.TokenSource.SAMM);
const FAKE_QUOTE_TOKEN = new token.Token("FAKEQUOTE", FAKE_QUOTE_ADDRESS, token.TokenSource.SAMM);
const FILL_ORDER_ID_1 = 3010;
const FILL_CLIENT_ORDER_ID_1 = 301;
const FILL_ORDER_ID_2 = 3020;
const FILL_CLIENT_ORDER_ID_2 = 302;

const DEFAULT_BUY_ORDER_ID = 201;
const DEFAULT_SELL_ORDER_ID = 202;
const DEFAULT_BUY_ORDER_PRICE = 99;
const DEFAULT_BUY_ORDER_SIZE = 0.1;

const DEFAULT_SPREAD_BEST_BID = 99;
const DEFAULT_SPREAD_BEST_ASK = 101;
const DEFAULT_SELL_ORDER_PRICE = 101;
const DEFAULT_SELL_ORDER_SIZE = 0.1;

const DEFAULT_FILLS = [
    {
        "eventFlags": {
            "fill": true,
            "out": false,
            "bid": false,
            "maker": true
        },
        "openOrdersSlot": 1,
        "feeTier": 0,
        "nativeQuantityReleased": 21705109,
        "nativeQuantityPaid": 2000,
        "nativeFeeOrRebate": 6509,
        "orderId": new Big(FILL_ORDER_ID_1),
        "openOrders": new solana.PublicKey("J3cy9dLEK7uvDoVuS11tW2GJWq3t7CUA6szVpr8fBqPH"),
        "clientOrderId": new Big(FILL_CLIENT_ORDER_ID_1),
        "side": "sell",
        "price": 10849.3,
        "feeCost": -0.006509,
        "size": 0.002
    }, {
        "eventFlags": {
            "fill": true,
            "out": false,
            "bid": false,
            "maker": true
        },
        "openOrdersSlot": 1,
        "feeTier": 0,
        "nativeQuantityReleased": 21705109,
        "nativeQuantityPaid": 2000,
        "nativeFeeOrRebate": 6509,
        "orderId": new Big(FILL_ORDER_ID_2),
        "openOrders": new solana.PublicKey("J3cy9dLEK7uvDoVuS11tW2GJWq3t7CUA6szVpr8fBqPH"),
        "clientOrderId": new Big(FILL_CLIENT_ORDER_ID_2),
        "side": "sell",
        "price": 10849.3,
        "feeCost": -0.006509,
        "size": 0.002
    }
];

function connection() {
    return {
        isConnection: true,
        waitFor: sinon.spy(),
        getTokenAccountBalance: sinon.fake.returns({
            value: {
                amount: 1000000000,
                decimals: 9
            }
        })
    };
}

function context() {
    const mutable = new ctx.Context();
    mutable.connection = connection();
    mutable.tokenCache = ctx.buildTokenCache(mutable);

    return mutable;
}

function auditor() {
    const fake = new audit.Auditor();
    fake.fill = sinon.spy();

    return fake;
}

function spread(bestBid, bestAsk) {
    const actualBestBid = bestBid || new Big(DEFAULT_SPREAD_BEST_BID);
    const actualBestAsk = bestAsk || new Big(DEFAULT_SPREAD_BEST_ASK);
    const mid = actualBestBid.add(actualBestAsk).div(new Big(numbers.HALF_DIVISOR));
    return new spreads.Spread(
        FAKE_BASE_TOKEN,
        FAKE_QUOTE_TOKEN,
        actualBestBid,
        [],
        actualBestAsk,
        [],
        mid
    );
}

function owner() {
    return new solana.Account();
}

function tokenAccountOwner() {
    const ownerAccount = randomAccount();
    return {
        executable: false,
        owner: ownerAccount.publicKey
    };
}

function tokenAccount() {
    const {publicKey} = randomAccount();
    const account = tokenAccountOwner();

    return new token.TokenAccount(publicKey, account);
}

function buyOrder(clientId) {
    const actualClientId = clientId ?
        new Big(clientId) :
        new Big(DEFAULT_BUY_ORDER_ID);
    return new order.Order(
        order.OrderSide.BUY,
        owner(),
        tokenAccount(),
        order.OrderType.POSTONLY,
        new Big(DEFAULT_BUY_ORDER_PRICE),
        new Big(DEFAULT_BUY_ORDER_SIZE),
        null,
        actualClientId
    );
}

function sellOrder(clientId) {
    const actualClientId = clientId ?
        new Big(clientId) :
        new Big(DEFAULT_SELL_ORDER_ID);
    return new order.Order(
        order.OrderSide.SELL,
        owner(),
        tokenAccount(),
        order.OrderType.POSTONLY,
        new Big(DEFAULT_SELL_ORDER_PRICE),
        new Big(DEFAULT_SELL_ORDER_SIZE),
        null,
        actualClientId
    );
}

function orders(buyClientId, sellClientId) {
    return [buyOrder(buyClientId), sellOrder(sellClientId)];
}

function serumOrders(buyClientId, sellClientId) {
    return orders(buyClientId, sellClientId).map((typedOrder) => typedOrder.toSerumOrder());
}

// eslint-disable-next-line max-statements
function market(baseSymbol = FAKE_BASE_TOKEN.symbol, quoteSymbol = FAKE_QUOTE_TOKEN.symbol) {
    const baseTokenAccount = tokenAccount();
    const quoteTokenAccount = tokenAccount();
    const name = `${baseSymbol}/${quoteSymbol}`;
    const fake = new mkts.Market(context(), name, randomPublicKey(), randomPublicKey(), mkts.MarketSource.TESTING);
    fake._baseTokenAccount = baseTokenAccount;
    fake._quoteTokenAccount = quoteTokenAccount;
    fake._baseSplTokenDecimals = FAKE_DECIMAL_PLACES;
    fake._quoteSplTokenDecimals = FAKE_DECIMAL_PLACES;
    fake.findOpenOrdersAccountsForOwner = sinon.fake.returns(Promise.resolve([]));
    fake.typedFindOpenOrdersAccountsForOwner = sinon.fake.returns(Promise.resolve([]));
    fake.settleFunds = sinon.fake.returns(Promise.resolve());
    fake.typedSettleFunds = sinon.fake.returns(Promise.resolve());
    fake.findBaseTokenAccountsForOwner = sinon.fake.returns(Promise.resolve([baseTokenAccount]));
    fake.findQuoteTokenAccountsForOwner = sinon.fake.returns(Promise.resolve([quoteTokenAccount]));
    fake.fetchSpread = sinon.fake.returns(Promise.resolve(spread()));
    fake.loadOrdersForOwner = sinon.spy(() => Promise.resolve(serumOrders()));
    fake.typedLoadOrdersForOwner = sinon.spy(() => Promise.resolve(orders()));
    fake.placeOrder = sinon.spy(() => Promise.resolve("fake-signature"));
    fake.typedPlaceOrder = sinon.spy(() => Promise.resolve("fake-signature"));
    fake.cancelOrder = sinon.spy(() => Promise.resolve("fake-signature"));
    fake.typedCancelOrder = sinon.spy(() => Promise.resolve("fake-signature"));
    fake.loadFills = sinon.spy(() => DEFAULT_FILLS);

    return fake;
}

async function accountTracker() {
    const mkt = market();
    return new accounttracker.SimulationAccountTracker(
        await mkt.findBaseTokenAccountsForOwner(),
        await mkt.findQuoteTokenAccountsForOwner(),
        false
    );
}

function balance(baseValue, quoteValue) {
    const baseBalance = new token.TokenValue(FAKE_BASE_TOKEN, baseValue, FAKE_DECIMAL_PLACES);
    const quoteBalance = new token.TokenValue(FAKE_QUOTE_TOKEN, quoteValue, FAKE_DECIMAL_PLACES);
    const balancePair = new balances.BalancePair(baseBalance, quoteBalance);
    const zeroBalance = new balances.BalancePair(
        new token.TokenValue(
            FAKE_BASE_TOKEN,
            numbers.Zero,
            FAKE_DECIMAL_PLACES
        ),
        new token.TokenValue(
            FAKE_QUOTE_TOKEN,
            numbers.Zero,
            FAKE_DECIMAL_PLACES
        )
    );
    return new balances.Balance(balancePair, balancePair, balancePair, zeroBalance);
}

function positionSizeProportion() {
    return new Big("0.001");
}

function fee() {
    return new Big("0.001");
}

function tickSize() {
    return new Big("0.0001");
}


class FakeSpreadFetcher extends spreads.SpreadFetcher {
    constructor(spreadToReturn) {
        super();
        this.spreadToReturn = spreadToReturn;
    }

    fetchSpread() {
        return this.spreadToReturn;
    }
}

function spreadFetcher(spreadToFetch) {
    return new FakeSpreadFetcher(spreadToFetch);
}

class FakeBalanceFetcher extends balances.BalanceFetcher {
    constructor(balanceToReturn) {
        super();
        this.balanceToReturn = balanceToReturn;
    }

    fetchBalance() {
        return this.balanceToReturn;
    }
}

function balanceFetcher(balanceToReturn) {
    let bal = balanceToReturn;
    const BTC_DECIMALS = 4;
    const USDT_DECIMALS = 1;
    if (!bal) {
        const baseTokenData = serum.TOKEN_MINTS.find((tok) => tok.name === "BTC");
        const baseToken = new token.Token(baseTokenData.name, baseTokenData.address, token.TokenSource.SERUMJS);
        const quoteTokenData = serum.TOKEN_MINTS.find((tok) => tok.name === "USDT");
        const quoteToken = new token.Token(quoteTokenData.name, quoteTokenData.address, token.TokenSource.SERUMJS);
        const baseBalance = new token.TokenValue(baseToken, numbers.One, BTC_DECIMALS);
        const quoteBalance = new token.TokenValue(quoteToken, numbers.One, USDT_DECIMALS);
        const balancePair = new balances.BalancePair(baseBalance, quoteBalance);
        const zeroBalance = new balances.BalancePair(
            new token.TokenValue(baseToken, numbers.Zero, BTC_DECIMALS),
            new token.TokenValue(quoteToken, numbers.Zero, USDT_DECIMALS)
        );
        bal = new balances.Balance(balancePair, balancePair, balancePair, zeroBalance);
    }

    return new FakeBalanceFetcher(bal);
}

function balanceSettler() {
    class FakeSettler extends settler.Settler {
    }
    const fakeSettler = new FakeSettler();
    fakeSettler.settle = sinon.spy();

    return fakeSettler;
}

function ordersMatch(expected, actual) {
    if (!expected) {
        return "expected order is undefined.";
    }

    if (!actual) {
        return "actual order is undefined.";
    }

    if (!expected.clientId.eq(actual.clientId)) {
        return `actual client ID of ${actual.clientId} does not match expected ${expected.clientId}`;
    }

    if (!expected.owner.publicKey.equals(actual.owner.publicKey)) {
        return `actual owner.publicKey of ${actual.owner.publicKey} does not match expected ${expected.owner.publicKey}`;
    }

    if (!expected.payer.equals(actual.payer)) {
        return `actual payer of ${actual.payer} does not match expected ${expected.payer}`;
    }

    if (expected.side !== actual.side) {
        return `actual side of ${actual.side} does not match expected ${expected.side}`;
    }

    if (!expected.price.eq(actual.price)) {
        return `actual price of ${actual.price} does not match expected ${expected.price}`;
    }

    if (!expected.size.eq(actual.size)) {
        return `actual size of ${actual.size} does not match expected ${expected.size}`;
    }

    if (expected.orderType !== actual.orderType) {
        return `actual order type of ${actual.orderType} does not match expected ${expected.orderType}`;
    }

    return true;
}

class FakeFillReporter extends fillreporter.FillReporter {
    // eslint-disable-next-line class-methods-use-this, no-empty-function
    reportOnFills() { }
}


function fillReporter() {
    return new FakeFillReporter();
}

class FakeOrderBuilder extends orderbuilder.OrderBuilder {
    constructor(buyOrdersToReturn, sellOrdersToReturn) {
        super();
        this.buyOrdersToReturn = buyOrdersToReturn || [];
        this.sellOrdersToReturn = sellOrdersToReturn || [];
    }

    buildOrders() {
        return Promise.resolve([...this.buyOrdersToReturn, ...this.sellOrdersToReturn]);
    }
}

function orderBuilder(buyOrdersToReturn, sellOrdersToReturn) {
    return new FakeOrderBuilder(buyOrdersToReturn, sellOrdersToReturn);
}

class FakeOrderPlacer extends orderplacer.OrderPlacer {
    placeOrders(ordersToPlace) {
        this.placedOrders = ordersToPlace;
        return Promise.resolve();
    }
}

function orderPlacer() {
    return new FakeOrderPlacer();
}

function openOrders(options) {
    const account = randomAccount();
    return new serum.OpenOrders(account.publicKey, options);
}

function tagger() {
    return {
        tag: sinon.spy((item) => item)
    };
}

module.exports = {
    randomAccount,
    randomPublicKey,
    FAKE_BASE_TOKEN,
    FAKE_QUOTE_TOKEN,
    context,
    auditor,
    connection,
    spread,
    market,
    owner,
    tokenAccountOwner,
    tokenAccount,
    buyOrder,
    sellOrder,
    orders,
    accountTracker,
    balance,
    positionSizeProportion,
    fee,
    tickSize,
    spreadFetcher,
    balanceFetcher,
    balanceSettler,
    ordersMatch,
    fillReporter,
    orderBuilder,
    orderPlacer,
    openOrders,
    tagger
};