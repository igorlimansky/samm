FROM node:14

ENV PATH="/home/node/app/bin:${PATH}"

COPY package*.json /home/node/app/
WORKDIR /home/node/app

RUN npm ci --only=production

COPY . /home/node/app/

CMD [ "balance" ]

USER node