"use strict";

const assert = require("./assert.js");
const ctx = require("./context.js");
const formatter = require("../lib/formatter.js");
const log = require("../lib/log.js");
const order = require("../lib/order.js");
const serum = require("@project-serum/serum");

class OrderPlacer {
    constructor() {
        assert.notConstructingAbstractType(OrderPlacer, new.target);

        this.isOrderPlacer = true;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    placeOrders() {
        assert.throwOnAbstractCall(OrderPlacer, this.placeOrders);
    }
}

class SimulationOrderPlacer extends OrderPlacer {
    constructor() {
        super();

        if (new.target === SimulationOrderPlacer) {
            Object.freeze(this);
        }
    }

    // eslint-disable-next-line class-methods-use-this
    placeOrders(orders) {
        assert.parameterTypeIsArrayOfType("SimulationOrderPlacer.placeOrders()", "orders", orders, order.Order);
        for (const orderToPlace of orders) {
            log.print("Simulation - not placing order:", formatter.order(orderToPlace));
        }

        return Promise.resolve();
    }
}

class AbstractMarketOrderPlacer extends OrderPlacer {
    constructor(context, market) {
        super();

        assert.parameterType("AbstractMarketOrderPlacer()", "context", context, ctx.Context);
        assert.parameterType("AbstractMarketOrderPlacer()", "market", market, serum.Market);

        this.context = context;
        this.market = market;

        if (new.target === AbstractMarketOrderPlacer) {
            Object.freeze(this);
        }
    }

    placeOrders() {
        assert.throwOnAbstractCall(AbstractMarketOrderPlacer, this.placeOrders);
    }
}

class AsyncOrderPlacer extends AbstractMarketOrderPlacer {
    constructor(context, market) {
        super(context, market);

        if (new.target === AsyncOrderPlacer) {
            Object.freeze(this);
        }
    }

    placeOrders(orders) {
        assert.parameterTypeIsArrayOfType("AsyncOrderPlacer.placeOrders()", "orders", orders, order.Order);

        const places = [];
        for (const orderToPlace of orders) {
            log.print("Placing order:", formatter.order(orderToPlace));
            places.push(this.market.typedPlaceOrder(orderToPlace));
        }

        return Promise.all(orders);
    }
}

class AsyncWaitingOrderPlacer extends AbstractMarketOrderPlacer {
    constructor(context, market) {
        super(context, market);

        if (new.target === AsyncWaitingOrderPlacer) {
            Object.freeze(this);
        }
    }

    placeOrders(orders) {
        assert.parameterTypeIsArrayOfType("AsyncWaitingOrderPlacer.placeOrders()", "orders", orders, order.Order);

        const places = [];
        for (const orderToPlace of orders) {
            log.print("Placing order:", formatter.order(orderToPlace));
            places.push(
                this.market.typedPlaceOrder(orderToPlace)
                    .then((signature) => {
                        log.print("Waiting on signature:", signature);
                        return this.context.connection.waitFor(signature);
                    })
            );
        }

        return Promise.all(places);
    }
}

class SyncOrderPlacer extends AbstractMarketOrderPlacer {
    constructor(context, market) {
        super(context, market);

        if (new.target === SyncOrderPlacer) {
            Object.freeze(this);
        }
    }

    async placeOrders(orders) {
        assert.parameterTypeIsArrayOfType("SyncWaitingOrderPlacer.placeOrders()", "orders", orders, order.Order);

        for (const orderToPlace of orders) {
            log.print("Placing order:", formatter.order(orderToPlace));
            // Yes, we really do want an await in a loop. This is a synchronous placing.
            // eslint-disable-next-line no-await-in-loop
            const signature = await this.market.typedPlaceOrder(orderToPlace);

            // Here too.
            // eslint-disable-next-line no-await-in-loop
            await this.context.connection.waitFor(signature);
            log.print("Order confirmed:", signature);
        }
    }
}

function createOrderPlacer(context, market) {
    if (context.simulate) {
        log.trace("Using SimulationOrderPlacer");
        return new SimulationOrderPlacer();
    } else if (context.asynchronous) {
        log.trace("Using AsyncOrderPlacer");
        return new AsyncOrderPlacer(context, market);
    } else if (context.asynchronousWaiting) {
        log.trace("Using AsyncWaitingOrderPlacer");
        return new AsyncWaitingOrderPlacer(context, market);
    }

    log.trace("Using SyncOrderPlacer");
    return new SyncOrderPlacer(context, market);
}

module.exports = {
    OrderPlacer,
    SimulationOrderPlacer,
    AbstractMarketOrderPlacer,
    AsyncOrderPlacer,
    AsyncWaitingOrderPlacer,
    SyncOrderPlacer,
    createOrderPlacer
};