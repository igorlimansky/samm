"use strict";

const accounttracker = require("../lib/accounttracker.js");
const assert = require("./assert.js");
const audit = require("../lib/auditor.js");
const ctx = require("./context.js");
const formatter = require("../lib/formatter.js");
const log = require("../lib/log.js");
const ordertracker = require("../lib/ordertracker.js");
const seen = require("./seen.js");
const serum = require("@project-serum/serum");

class FillReporter {
    constructor() {
        assert.notConstructingAbstractType(FillReporter, new.target);

        this.isFillReporter = true;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    reportOnFills() {
        assert.throwOnAbstractCall(FillReporter, this.reportOnFills);
    }
}

class AuditingFillReporter extends FillReporter {
    constructor(context, market, auditor, accountTracker, orderTracker) {
        super();

        assert.parameterType("AuditingFillReporter()", "context", context, ctx.Context);
        assert.parameterType("AuditingFillReporter()", "market", market, serum.Market);
        assert.parameterType("AuditingFillReporter()", "auditor", auditor, audit.Auditor);
        assert.parameterType("AuditingFillReporter()", "accountTracker", accountTracker, accounttracker.AccountTracker);
        assert.parameterType("AuditingFillReporter()", "orderTracker", orderTracker, ordertracker.OrderTracker);

        this.market = market;
        this.auditor = auditor;
        this.accountTracker = accountTracker;
        this.orderTracker = orderTracker;
        this.context = context;
        this.seen = new seen.Seen(seen.DEFAULT_SEEN_BUFFER_SIZE);

        if (new.target === AuditingFillReporter) {
            Object.freeze(this);
        }
    }

    async reportOnFills() {
        const fills = await this.market.typedLoadFills(this.context.limit);
        if (fills.length === 0) {
            log.trace(`No fills in market ${this.market.name}`);
        } else {
            log.print(`Fills in market ${this.market.name}:`);
            for (const fill of fills) {
                if (this.accountTracker.isOneOfMyOpenOrdersAccounts(fill.openOrders)) {
                    if (!this.seen.alreadySeen(fill.clientOrderId)) {
                        log.print("Fill:", fill.openOrders.toString(), formatter.fill(fill));
                        const originalOrder = this.orderTracker.getOrderByClientId(fill.clientOrderId);
                        this.auditor.fill(fill, originalOrder);
                        this.seen.markSeen(fill.clientOrderId);
                    }
                }
            }
        }
    }
}

module.exports = {
    FillReporter,
    AuditingFillReporter
};