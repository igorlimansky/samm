"use strict";

function isFalse(result, message) {
    if (result) {
        throw new TypeError(`Assertion failure: '${message}'.`);
    }
}

function isTrue(result, message) {
    if (!result) {
        throw new TypeError(`Assertion failure: '${message}'.`);
    }
}

function notConstructingAbstractType(type, targetType) {
    if (targetType === type) {
        throw new TypeError(`Cannot construct abstract type '${type?.name}' directly.`);
    }
}

function objectType(caller, variableName, variable, type) {
    if (!(variable instanceof type)) {
        throw new TypeError(`${caller} object error - ${variableName} '${variable}' is of type '${variable?.constructor?.name}', not of type '${type?.name}'.`);
    }
}

function parameterIsDefined(caller, variableName, variable) {
    if (!variable) {
        throw new TypeError(`${caller} parameter error - ${variableName} '${variable}' is not defined.`);
    }
}

function parameterType(caller, variableName, variable, type) {
    // Sometimes we'll get a CompoundItem. It behaves like a Type but it isn't strictly a
    // Type. We want to accept that, and we know that CompoundItem.type has the underlying Type,
    // so we use that.
    if (!(variable instanceof type) && !(variable?.type === type)) {
        throw new TypeError(`${caller} parameter error - ${variableName} '${variable}' is of type '${variable?.constructor?.name}', not of type '${type?.name}'.`);
    }
}

function parameterTypeHasFunctionNamed(caller, variableName, variable, functionName) {
    if (!variable[functionName] || typeof variable[functionName] !== "function") {
        throw new TypeError(`${caller} parameter error - ${variableName} '${variable}' is of type '${variable?.constructor?.name}' and does not have a function called '${functionName}'.`);
    }
}

function parameterTypeIsEnum(caller, variableName, variable, type) {
    // Can't use the concrete types enums.Enum and enums.EnumValue here because we'd
    // have a dependency cycle - assert using enum types, enums using assertions...
    if (variable?.constructor?.name !== "EnumValue") {
        throw new TypeError(`${caller} parameter error - ${variableName} '${variable}' is of type '${variable?.constructor?.name}', not of type 'enums.EnumValue'.`);
    }

    if (type?.constructor?.name !== "Enum") {
        throw new TypeError(`${caller} parameter error - ${variableName} type check is of type '${type?.constructor?.name}', not of type 'enums.Enum'.`);
    }

    if (!type.hasValue(variable)) {
        throw new TypeError(`${caller} parameter error - ${variableName} '${variable}' is not a member of enumeration type '${type?.constructor?.name}'.`);
    }
}

function parameterTypeIsConnection(caller, variableName, variable) {
    if (!variable.isConnection) {
        throw new TypeError(`${caller} parameter error - ${variableName} '${variable}' is of type '${variable?.constructor?.name}', not of type 'solana.Connection'.`);
    }
}

function parameterTypeIsContext(caller, variableName, variable) {
    if (!variable.isContext) {
        throw new TypeError(`${caller} parameter error - ${variableName} '${variable}' is of type '${variable?.constructor?.name}', not of type context.`);
    }
}

function parameterTypeIsString(caller, variableName, variable) {
    if (typeof variable !== "string" && !(variable instanceof String)) {
        throw new TypeError(`${caller} parameter error - ${variableName} '${variable}' is of type '${typeof variable}', not of type 'String'.`);
    }
}

function parameterTypeOf(caller, variableName, variable, typeName) {
    if (typeof variable !== typeName) {
        throw new TypeError(`${caller} parameter error - ${variableName} '${variable}' is of type '${typeof variable}', not of type '${typeName}'.`);
    }
}

function parameterTypeOrUndefined(caller, variableName, variable, type) {
    if (variable && !(variable instanceof type)) {
        throw new TypeError(`${caller} parameter error - ${variableName} '${variable}' is of type '${variable?.constructor?.name}', not of type '${type?.name}'.`);
    }
}

function parameterTypeIsArray(caller, variableName, variable) {
    if (!Array.isArray(variable)) {
        throw new Error(`${caller} parameter error -  ${variableName} '${variable}' is not an array.`);
    }
}

function parameterTypeIsArrayOfType(caller, variableName, variable, type) {
    parameterTypeIsArray(caller, variableName, variable);

    for (let index = 0; index < variable.length; index += 1) {
        if (type === String) {
            parameterTypeIsString(caller, `${variableName}[${index}]`, variable[index]);
        } else {
            parameterType(caller, `${variableName}[${index}]`, variable[index], type);
        }
    }
}

function throwOnAbstractCall(type, func) {
    const typeName = type.name || "«un-named-type»";
    const funcName = func.name || "«un-named-function»";
    throw new TypeError(`Cannot call ${funcName}() on abstract ${typeName}.`);
}

module.exports = {
    isFalse,
    isTrue,
    notConstructingAbstractType,
    objectType,
    parameterIsDefined,
    parameterType,
    parameterTypeHasFunctionNamed,
    parameterTypeIsEnum,
    parameterTypeIsConnection,
    parameterTypeIsContext,
    parameterTypeIsString,
    parameterTypeOf,
    parameterTypeOrUndefined,
    parameterTypeIsArray,
    parameterTypeIsArrayOfType,
    throwOnAbstractCall
};