"use strict";

const assert = require("./assert");
const ctx = require("./context");
const serum = require("@project-serum/serum");
const solana = require("@solana/web3.js");

const CACHE_ACCOUNT_ADDRESS_MS = 3600000;

class OrderFetcher {
    constructor() {
        assert.notConstructingAbstractType(OrderFetcher, new.target);

        this.isOrderFetcher = true;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    fetchExistingOrders() {
        assert.throwOnAbstractCall(OrderFetcher, this.fetchExistingOrders);
    }
}

class SimulationOrderFetcher extends OrderFetcher {
    constructor() {
        super();

        if (new.target === SimulationOrderFetcher) {
            Object.freeze(this);
        }
    }

    // eslint-disable-next-line class-methods-use-this
    fetchExistingOrders() {
        return Promise.resolve([]);
    }
}

class SyncOrderFetcher extends OrderFetcher {
    constructor(context, market, owner) {
        super();

        assert.parameterType("SyncOrderFetcher()", "context", context, ctx.Context);
        assert.parameterType("SyncOrderFetcher()", "market", market, serum.Market);
        assert.parameterType("SyncOrderFetcher()", "owner", owner, solana.Account);

        this.context = context;
        this.market = market;
        this.owner = owner;

        if (new.target === SyncOrderFetcher) {
            Object.freeze(this);
        }
    }

    fetchExistingOrders() {
        return this.market.typedLoadOrdersForOwner(
            this.owner.publicKey,
            CACHE_ACCOUNT_ADDRESS_MS
        );
    }
}

function createOrderFetcher(context, market, owner) {
    if (context.simulate) {
        return new SimulationOrderFetcher();
    }

    return new SyncOrderFetcher(context, market, owner);
}

module.exports = {
    OrderFetcher,
    SimulationOrderFetcher,
    SyncOrderFetcher,
    createOrderFetcher
};