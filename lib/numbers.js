"use strict";

const Big = require("big.js");
const BN = require("bn.js");

const DECIMAL_RADIX = 10;
const HEX_RADIX = 16;
const HALF_DIVISOR = 2;
const MILLISECONDS_TO_SECONDS_FACTOR = 1000;
const PERCENTAGE_FACTOR = 100;

const BYTE_TO_HEX_MAX_CHARACTERS = 2;
const BYTE_TO_HEX_MAX_CHARACTERS_OFFSET = 0 - BYTE_TO_HEX_MAX_CHARACTERS;

const DEFAULT_PERCENTAGE_DECIMAL_PLACES = 2;
const DEFAULT_PRICE_DECIMAL_PLACES = 2;
const DEFAULT_FEE_DECIMAL_PLACES = 2;
const DEFAULT_POSITION_SIZE_DECIMAL_PLACES = 4;

const Zero = new Big(0);
const One = new Big(1);

const BNZero = new BN(0);
const BNOne = new BN(1);

function parseIntWithDefault(value, defaultValue) {
    return parseInt(value, DECIMAL_RADIX) || defaultValue;
}

function percentageToProportion(value, defaultValue) {
    return new Big(value || defaultValue).div(PERCENTAGE_FACTOR);
}

function toPercentageString(value, places) {
    const valuePercentage = value.times(PERCENTAGE_FACTOR).toFixed(places || DEFAULT_PERCENTAGE_DECIMAL_PLACES);
    return `${valuePercentage}%`;
}

function toInt(value) {
    return parseInt(value, DECIMAL_RADIX);
}

function createBigFromNumberAndDecimals(num, decimals) {
    const noDecimals = new Big(num);
    const decimalPower = new Big(DECIMAL_RADIX).pow(decimals);
    return noDecimals.div(decimalPower);
}

function createBigFromBNAndDecimals(bn, decimals) {
    if (decimals) {
        const noDecimals = new Big(bn.toString(DECIMAL_RADIX));
        const decimalPower = new Big(DECIMAL_RADIX).pow(decimals);
        return noDecimals.div(decimalPower);
    }

    return new Big(bn.toString(DECIMAL_RADIX));
}

function createBigFromBNBuffer(buffer) {
    const bn = new BN(
        [...buffer]
            .reverse()
            .map((value) => `00${value.toString(HEX_RADIX)}`.slice(BYTE_TO_HEX_MAX_CHARACTERS_OFFSET))
            .join(""),
        HEX_RADIX
    );

    return createBigFromBNAndDecimals(bn);
}

function createBigFromBalanceResult(balanceResult) {
    return createBigFromNumberAndDecimals(
        balanceResult.value.amount, balanceResult.value.decimals
    );
}

function convertBNToBigOrUndefined(bn) {
    if (bn) {
        return new Big(bn.toString(DECIMAL_RADIX));
    }

    // eslint-disable-next-line no-undefined
    return undefined;
}

function createBNFromBigAndDecimals(value, decimals) {
    if (decimals) {
        const decimalPower = new Big(DECIMAL_RADIX).pow(decimals);
        const noDecimals = value.times(decimalPower);
        const integer = noDecimals.round(0);
        return new BN(integer.toString());
    }

    return new BN(value.toFixed(0));
}

function isZero(number) {
    if (number === 0) {
        return true;
    }

    if (number) {
        return Zero.eq(number) || BNZero.eq(number);
    }

    return false;
}

function isZeroOrUndefined(number) {
    if (number === 0) {
        return true;
    }

    if (number) {
        return Zero.eq(number) || BNZero.eq(number);
    }

    return true;
}

function isBN(value) {
    return BN.isBN(value);
}

function negate(number) {
    return Zero.minus(number);
}

module.exports = {
    DECIMAL_RADIX,
    HALF_DIVISOR,
    MILLISECONDS_TO_SECONDS_FACTOR,
    PERCENTAGE_FACTOR,
    DEFAULT_PERCENTAGE_DECIMAL_PLACES,
    DEFAULT_PRICE_DECIMAL_PLACES,
    DEFAULT_FEE_DECIMAL_PLACES,
    DEFAULT_POSITION_SIZE_DECIMAL_PLACES,
    Zero,
    One,
    BNZero,
    BNOne,
    parseIntWithDefault,
    percentageToProportion,
    toPercentageString,
    toInt,
    createBigFromNumberAndDecimals,
    createBigFromBNAndDecimals,
    createBigFromBNBuffer,
    createBigFromBalanceResult,
    createBNFromBigAndDecimals,
    convertBNToBigOrUndefined,
    isZero,
    isZeroOrUndefined,
    isBN,
    negate
};