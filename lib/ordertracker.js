"use strict";

const assert = require("./assert.js");
const array = require("./array.js");
const Big = require("big.js");
const log = require("../lib/log.js");
const order = require("./order.js");

// A client ID of 0 is ignored so we always start at 1
class IntegerCounter {
    constructor(startAt) {
        if (startAt > Number.MAX_SAFE_INTEGER) {
            this._counter = 0;
        } else {
            this._counter = startAt || 0;
        }
    }

    increment() {
        if (this._counter === Number.MAX_SAFE_INTEGER) {
            this._counter = 1;
        } else {
            this._counter += 1;
        }

        return this._counter;
    }
}

const START_AT_ALIGNMENT = 10000;

class OrderTracker {
    static randomStartAt() {
        const seed = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);

        // When orders are scrolling past it can be nice to keep track of the client IDs.
        // This is easier if they're small (like '10' or '27'). But we want a random startAt
        // so that we don't clash with someone else. So we create a (probably large) random
        // integer and 'align' it to 10,000. That'll give us clientIds like 8673937200016.
        const aligner = seed % START_AT_ALIGNMENT;
        const start = seed - aligner;
        return new OrderTracker(start);
    }

    constructor(startAt) {
        this.isOrderTracker = true;

        // A client ID of 0 is ignored.
        this._counter = new IntegerCounter(startAt);

        this._active = [];
        this._orders = [];
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    clearTrackedOrders() {
        this._orders = [];
    }

    trackOrders(orders) {
        assert.parameterTypeIsArrayOfType("OrderTracker.trackOrders()", "orders", orders, order.Order);
        for (const orderToTrack of orders) {
            this._orders.push(orderToTrack);
        }
    }

    getOrderByClientId(clientId) {
        assert.parameterType("OrderTracker.getOrderByClientId()", "clientId", clientId, Big);

        for (const trackedOrder of this._orders) {
            if (trackedOrder.clientId.eq(clientId)) {
                return trackedOrder;
            }
        }

        log.warn(`Could not find active order with client ID ${clientId}`);
        return null;
    }

    _isTrackedClientId(idToCheck) {
        for (const trackedOrder of this._orders) {
            if (trackedOrder.clientId.eq(idToCheck)) {
                return true;
            }
        }

        return false;
    }

    nextClientId() {
        return new Big(this._counter.increment());
    }

    get trackedIds() {
        return this._orders.map((trackedOrder) => trackedOrder.clientId);
    }

    allIdsTracked(idsToCheck) {
        assert.parameterTypeIsArrayOfType("OrderTracker.allIdsTracked()", "idsToCheck", idsToCheck, Big);

        if (this._orders.length !== idsToCheck.length) {
            return false;
        }

        const tracked = this.trackedIds;
        return array.hasAllMembersOrderIndependent(tracked, idsToCheck, "eq");
    }

    toString() {
        const tracked = this._orders.map((trackedOrder) => trackedOrder.clientId.toString()).join(", ");
        return `« OrderTracker - tracking [${tracked}] [last ID: ${this._counter._counter}] »`;
    }
}

module.exports = {
    OrderTracker
};