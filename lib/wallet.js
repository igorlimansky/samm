"use strict";

const assert = require("../lib/assert.js");
const bip32 = require("bip32");
const bip39 = require("bip39");
const file = require("../lib/file.js");
const nacl = require("tweetnacl");
const solana = require("@solana/web3.js");
const tags = require("../lib/tags.js");

const JSON_INDENTATION = 4;

class Wallet {
    constructor(mnemonicSeedWords, options) {
        assert.parameterTypeIsString("Wallet()", "mnemonicSeedWords", mnemonicSeedWords);

        this.isWallet = true;

        this.options = options || {};

        if (!bip39.validateMnemonic(mnemonicSeedWords)) {
            throw new Error("Invalid menomic seed words");
        }

        this.mnemonicSeedWords = mnemonicSeedWords;

        tags.ensureTags(this);

        if (new.target === Wallet) {
            Object.freeze(this);
        }
    }

    secretKey() {
        const walletIndex = this.options.walletIndex || 0;
        const accountIndex = this.options.accountIndex || 0;

        const seed = bip39.mnemonicToSeedSync(this.mnemonicSeedWords);
        const derivedSeed = bip32.fromSeed(seed).derivePath(`m/501'/${walletIndex}'/0/${accountIndex}`).privateKey;

        return nacl.sign.keyPair.fromSeed(derivedSeed).secretKey;
    }

    save(filename) {
        const data = {
            "mnemonic": this.mnemonicSeedWords
        };
        const jsonData = JSON.stringify(data, null, JSON_INDENTATION);
        file.writeFile(filename, jsonData, this.options.overwrite);
    }

    get account() {
        const secretKey = this.secretKey();
        return new solana.Account(secretKey);
    }
}

function createWallet(options) {
    const mnemonic = bip39.generateMnemonic();
    return new Wallet(mnemonic, options);
}

function loadWallet(options) {
    const {
        filename
    } = options;

    const jsonData = file.readFile(filename);
    const data = JSON.parse(jsonData);
    return new Wallet(data.mnemonic, options);
}

module.exports = {
    Wallet,
    createWallet,
    loadWallet
};