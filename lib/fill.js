"use strict";

const Big = require("big.js");
const numbers = require("./numbers.js");
const order = require("./order.js");
const tags = require("./tags.js");

// A fill from Serum usually looks something like as an object:
// {
//     eventFlags: { fill: true, out: false, bid: false, maker: true },
//     openOrdersSlot: 2,
//     feeTier: 6,
//     nativeQuantityReleased: <BN: 76b862c8>,
//     nativeQuantityPaid: <BN: 4c4b40>,
//     nativeFeeOrRebate: <BN: f3048>,
//     orderId: <BN: 9b8800000000002e2b70>,
//     openOrders: PublicKey {
//       _bn: <BN: a2c8cf37e7e980c0b0279ce2a0d78f51b19d3647e3ae3f7b6df49a07da52050a>
//     },
//     clientOrderId: <BN: 16400e67419e9fb2>,
//     side: 'sell',
//     price: 398.16,
//     feeCost: -0.9954,
//     size: 5
// }
// And as JSON:
// {
//     "eventFlags": {
//         "fill": true,
//         "out": false,
//         "bid": false,
//         "maker": true
//     },
//     "openOrdersSlot": 2,
//     "feeTier": 6,
//     "nativeQuantityReleased": "76b862c8",
//     "nativeQuantityPaid": "4c4b40",
//     "nativeFeeOrRebate": "0f3048",
//     "orderId": "9b8800000000002e2b70",
//     "openOrders": {
//         "_bn": "a2c8cf37e7e980c0b0279ce2a0d78f51b19d3647e3ae3f7b6df49a07da52050a"
//     },
//     "clientOrderId": "16400e67419e9fb2",
//     "side": "sell",
//     "price": 398.16,
//     "feeCost": -0.9954,
//     "size": 5
// }
//
// After working through some fills, nativeQuantityPaid and nativeQuantityReleased aren't
// expressed in Base or Quote, the currency switches depending on whether you are buying
// or selling.
//
// * nativeQuantityPaid - tokens you send. For market Base/Quote, if you BUY you send Quote,
//   if you SELL you send Base.
// * nativeQuantityReleased - tokens you receive. For market Base/Quote, if you BUY you receive
//   Base, if you SELL you receive Quote.
// * nativeFeeOrRebate - tokens you pay or are paid. For market Base/Quote, these are always
//   denominated in Quote since these are calculated as a percentage of SIZE * PRICE, and
//   PRICE is denominated in Quote.
class Fill {
    static fromSerum(untyped) {
        const clone = {
            ...untyped
        };

        clone.orderId = numbers.convertBNToBigOrUndefined(untyped.orderId);
        clone.clientOrderId = numbers.convertBNToBigOrUndefined(untyped.clientOrderId);
        clone.nativeQuantityReleased = numbers.convertBNToBigOrUndefined(untyped.nativeQuantityReleased);
        clone.nativeQuantityPaid = numbers.convertBNToBigOrUndefined(untyped.nativeQuantityPaid);
        clone.nativeFeeOrRebate = numbers.convertBNToBigOrUndefined(untyped.nativeFeeOrRebate);

        clone.side = order.OrderSide.fromName(untyped.side);
        clone.price = new Big(untyped.price);
        clone.feeCost = new Big(untyped.feeCost);
        clone.size = new Big(untyped.size);

        const keys = [clone.openOrders.toString(), clone.orderId.toFixed(0), clone.nativeQuantityReleased.toFixed(0)];
        clone.key = keys.join("+");

        return new Fill(clone);
    }

    static fromSerumArray(untypeds) {
        return untypeds.map((untyped) => Fill.fromSerum(untyped));
    }

    constructor(
        untyped
    ) {
        Object.assign(this, untyped);

        this.isFill = true;

        tags.ensureTags(this);
        this.tags.addFlags(this.eventFlags);

        Object.freeze(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    toString() {
        const id = this.clientOrderId || this.orderId || "<No ID>";
        return `« Fill ${this.side} ${this.size} at price ${this.price} [${id}] »`;
    }
}

module.exports = {
    Fill
};