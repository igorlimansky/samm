"use strict";

const chalk = require("chalk");
const file = require("../lib/file.js");

const _options = {
    trace: false,
    print: true,
    warn: true,
    error: true,
    critical: true
};

const _loggers = [];

function setLevels({
    trace,
    print,
    warn,
    error,
    critical
}) {
    if (trace === true) {
        _options.trace = true;
    } else if (trace === false) {
        _options.trace = false;
    }

    if (print === true) {
        _options.print = true;
    } else if (print === false) {
        _options.print = false;
    }

    if (warn === true) {
        _options.warn = true;
    } else if (warn === false) {
        _options.warn = false;
    }

    if (error === true) {
        _options.error = true;
    } else if (error === false) {
        _options.error = false;
    }

    if (critical === true) {
        _options.critical = true;
    } else if (critical === false) {
        _options.critical = false;
    }
}

function addLogger(logger) {
    _loggers.push(logger);
}

function logTrace(...args) {
    if (!_options.trace) {
        return;
    }

    for (const logger of _loggers) {
        if (logger.trace) {
            logger.trace(...args);
        }
    }
}

function logPrint(...args) {
    if (!_options.print) {
        return;
    }

    for (const logger of _loggers) {
        if (logger.print) {
            logger.print(...args);
        }
    }
}

function logWarn(...args) {
    if (!_options.warn) {
        return;
    }

    for (const logger of _loggers) {
        if (logger.warn) {
            logger.warn(...args);
        }
    }
}

function logError(...args) {
    if (!_options.error) {
        return;
    }

    for (const logger of _loggers) {
        if (logger.error) {
            logger.error(...args);
        }
    }
}

function logCritical(...args) {
    if (!_options.critical) {
        return;
    }

    for (const logger of _loggers) {
        if (logger.critical) {
            logger.critical(...args);
        }
    }
}

function consoleLogger() {
    function timestamp() {
        return chalk.yellow(new Date().toISOString());
    }

    return {
        isLogger: true,

        trace: function trace(...args) {
            // eslint-disable-next-line no-console
            console.log(timestamp(), "📜", ...args);
        },

        print: function print(...args) {
            // eslint-disable-next-line no-console
            console.info(timestamp(), "📄", ...args);
        },

        warn: function warn(...args) {
            // eslint-disable-next-line no-console
            console.warn(timestamp(), "⚠️", ...args);
        },

        error: function error(...args) {
            // eslint-disable-next-line no-console
            console.group(timestamp(), "🚨", "Error:");
            // eslint-disable-next-line no-console
            console.error(...args);
            // eslint-disable-next-line no-console
            console.groupEnd();
        },

        critical: function critical(...args) {
            // eslint-disable-next-line no-console
            console.group(timestamp(), "🛑", "Error:");
            // eslint-disable-next-line no-console
            console.error(...args);
            // eslint-disable-next-line no-console
            console.groupEnd();
        }
    };
}

_loggers.push(consoleLogger());

function buildFileLogger(filepath, levels) {
    const logger = {
        isLogger: true
    };
    for (const level of levels) {
        logger[level] = function loggerLevel(...args) {
            const timestamp = new Date().toISOString();
            let toLog = `${timestamp} - ${args}\n\n`;
            if (args[0] instanceof Error) {
                const err = args[0];
                toLog = `${timestamp} - ${err}\n${err?.stack}\n\n`;
            }

            file.appendFile(filepath, toLog);
        };
    }

    return logger;
}

module.exports = {
    setLevels,
    addLogger,
    buildFileLogger,
    trace: logTrace,
    print: logPrint,
    warn: logWarn,
    error: logError,
    critical: logCritical
};