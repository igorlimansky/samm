"use strict";

const assert = require("../lib/assert.js");
const Twitter = require("twitter-lite");

function buildTwitterOauthClient(context) {
    assert.parameterTypeIsContext("buildTwitterOauthClient()", "context", context);

    if (!context.twitterConsumerKey
        || !context.twitterConsumerSecret) {
        throw new Error(`Required Twitter authorisation enviroment variable not present. Need all of:
parameter --twitterConsumerKey or environment variable TWITTER_CONSUMER_KEY
parameter --twitterConsumerSecret or environment variable TWITTER_CONSUMER_SECRET
        
Please see the following pages for details on what to provide:
https://developer.twitter.com/en/docs/authentication/api-reference/authenticate
https://developer.twitter.com/en/docs/authentication/api-reference/request_token
https://developer.twitter.com/en/docs/authentication/api-reference/access_token
`);
    }

    return new Twitter({
        // eslint-disable-next-line camelcase
        consumer_key: context.twitterConsumerKey,
        // eslint-disable-next-line camelcase
        consumer_secret: context.twitterConsumerSecret,
        // eslint-disable-next-line camelcase
        access_token_key: context.twitterAccessTokenKey,
        // eslint-disable-next-line camelcase
        access_token_secret: context.twitterAccessTokenSecret
    });
}

function buildTwitterClient(context) {
    assert.parameterTypeIsContext("buildTwitterClient()", "context", context);

    if (!context.twitterConsumerKey
        || !context.twitterConsumerSecret
        || !context.twitterAccessTokenKey
        || !context.twitterAccessTokenSecret) {
        throw new Error(`Required Twitter authorisation enviroment variable not present. Need all of:
parameter --twitterConsumerKey or environment variable TWITTER_CONSUMER_KEY
parameter --twitterConsumerSecret or environment variable TWITTER_CONSUMER_SECRET
parameter --twitterAccessTokenKey or environment variable TWITTER_ACCESS_TOKEN_KEY
parameter --twitterAccessTokenSecret or environment variable TWITTER_ACCESS_TOKEN_SECRET

Please see the following pages for details on what to provide:
https://developer.twitter.com/en/docs/authentication/api-reference/authenticate
https://developer.twitter.com/en/docs/authentication/api-reference/request_token
https://developer.twitter.com/en/docs/authentication/api-reference/access_token
`);
    }

    return new Twitter({
        // eslint-disable-next-line camelcase
        consumer_key: context.twitterConsumerKey,
        // eslint-disable-next-line camelcase
        consumer_secret: context.twitterConsumerSecret,
        // eslint-disable-next-line camelcase
        access_token_key: context.twitterAccessTokenKey,
        // eslint-disable-next-line camelcase
        access_token_secret: context.twitterAccessTokenSecret
    });
}

function sendUsingClient(client, message) {
    assert.parameterType(client, "client", client, Twitter);
    assert.parameterTypeIsString("sendUsingContext()", "message", message);

    return client.post("statuses/update", {
        status: message,
        // eslint-disable-next-line camelcase
        auto_populate_reply_metadata: true
    });
}

function sendUsingContext(context, message) {
    assert.parameterTypeIsContext("sendUsingContext()", "context", context);
    assert.parameterTypeIsString("sendUsingContext()", "message", message);

    const client = buildTwitterClient(context);
    return sendUsingClient(client, message);
}

module.exports = {
    buildTwitterOauthClient,
    buildTwitterClient,
    sendUsingClient,
    sendUsingContext
};