"use strict";

const argParser = require("minimist");
const array = require("../lib/array.js");
const connector = require("../lib/connector.js");
const ensureArray = require("ensure-array");
const file = require("../lib/file.js");
const log = require("../lib/log.js");
const mkts = require("./markets.js");
const numbers = require("../lib/numbers.js");
const solana = require("@solana/web3.js");
const tags = require("./tags.js");
const token = require("./token.js");
const wallet = require("../lib/wallet.js");

const IGNORE_ARGV_COMMAND_NAME = 2;
const DEFAULT_NUMBER_OF_CONFIRMATIONS_TO_WAIT = 6;
const DEFAULT_FEE_PERCENTAGE = "0.3";
const DEFAULT_POSITION_SIZE_PERCENTAGE = "0.1";
const DEFAULT_POLL_INTERVAL_SECONDS = 30;
const DEFAULT_LIMIT = 100;
const DEFAULT_REFRESH_SETTLER_EVERY = 20;
const FIRST_RETRY_DELAY = 1;
const SECOND_RETRY_DELAY = 3;
const THIRD_RETRY_DELAY = 6;
const DEFAULT_RETRY_DELAYS = [FIRST_RETRY_DELAY, SECOND_RETRY_DELAY, THIRD_RETRY_DELAY];

const ALLOWED_COMMITMENTS = [
    "max",
    "recent",
    "root",
    "single",
    "singleGossip"
];

const CONTEXT_DEFAULTS = {
    additionalMarket: [],
    additionalMarketFile: "markets.json",
    additionalToken: [],
    additionalTokenFile: "tokens.json",
    asynchronousWaiting: false,
    auditLogDirectory: "auditlogs",
    commitment: "single",
    confirmations: DEFAULT_NUMBER_OF_CONFIRMATIONS_TO_WAIT,
    feeProportion: numbers.percentageToProportion(DEFAULT_FEE_PERCENTAGE),
    filename: "wallet.json",
    host: "https://api.mainnet-beta.solana.com",
    limit: DEFAULT_LIMIT,
    name: "SAMM",
    orderType: "limit",
    pollInterval: DEFAULT_POLL_INTERVAL_SECONDS,
    pollIntervalSeconds: DEFAULT_POLL_INTERVAL_SECONDS,
    pollIntervalMilliseconds: DEFAULT_POLL_INTERVAL_SECONDS * numbers.MILLISECONDS_TO_SECONDS_FACTOR,
    poolRoot: "9qvG1zUp8xF1Bi4m6UdRNby1BAAuaDrUxSpv4CmRRMjL",
    positionSizeProportion: numbers.percentageToProportion(DEFAULT_POSITION_SIZE_PERCENTAGE),
    refreshSettlerEvery: DEFAULT_REFRESH_SETTLER_EVERY,
    retries: DEFAULT_RETRY_DELAYS,
    trace: false,
    twitterConsumerKey: process.env.TWITTER_CONSUMER_KEY,
    twitterConsumerSecret: process.env.TWITTER_CONSUMER_SECRET,
    twitterAccessTokenKey: process.env.TWITTER_ACCESS_TOKEN_KEY,
    twitterAccessTokenSecret: process.env.TWITTER_ACCESS_TOKEN_SECRET
};

// This function removes some command-line parameters that are replaced by strongly-typed
// versions. These are removed to avoid confusion and accidental use.
function cleanupContext(context) {
    delete context.verbose;
    delete context.feePercentage;
    delete context.pollInterval;
    delete context.positionSizePercentage;
    delete context.additionalToken;
    delete context.additionalMarket;
}

function createTaggers(context) {
    return [
        new tags.FunctionTagger((item) => tags.addTag(item, `context:feeProportion:${context.feeProportion}`)),
        new tags.FunctionTagger((item) => tags.addTag(item, `context:positionSizeProportion:${context.positionSizeProportion}`))
    ];
}

class Context {
    constructor() {
        this.isContext = true;

        Object.assign(this, CONTEXT_DEFAULTS);

        this.tagger = new array.CompoundItem(tags.Tagger, "tag", ...createTaggers(this));
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    get currentMarketOrThrow() {
        return this.currentMarket || (() => {
            throw new Error("No market specified");
        })();
    }

    marketOrThrow() {
        const marketName = this.market;
        if (!marketName) {
            throw new Error("No market specified");
        }

        return marketName;
    }

    loadWallet() {
        return wallet.loadWallet(this);
    }

    loadAccount() {
        const wal = this.loadWallet();
        return wal.account;
    }
}

function buildTokenCache(context) {
    const tokenCache = token.loadTokenCache();
    if (file.exists(context.additionalTokenFile)) {
        const jsonData = file.readFile(context.additionalTokenFile);
        const data = JSON.parse(jsonData);
        for (const additionalToken of data) {
            const address = new solana.PublicKey(additionalToken.address);
            const tokenToAdd = new token.Token(additionalToken.name, address, token.TokenSource.ADDITIONALTOKENFILE);
            tokenCache.add(tokenToAdd);
        }
    }

    for (const additionalToken of ensureArray(context.additionalToken)) {
        const [symbol, addressString] = additionalToken.split(":");
        const address = new solana.PublicKey(addressString);
        const tokenToAdd = new token.Token(symbol, address, token.TokenSource.COMMANDLINE);
        tokenCache.add(tokenToAdd);
    }

    return tokenCache;
}

function buildMarketCache(context) {
    const marketCache = mkts.loadMarketCache(context);
    if (file.exists(context.additionalMarketFile)) {
        const jsonData = file.readFile(context.additionalMarketFile);
        const data = JSON.parse(jsonData);
        for (const additionalMarket of data) {
            if (!additionalMarket.deprecated) {
                const marketToAdd = new mkts.Market(
                    context,
                    additionalMarket.name,
                    new solana.PublicKey(additionalMarket.address),
                    new solana.PublicKey(additionalMarket.programId),
                    mkts.MarketSource.ADDITIONALMARKETFILE
                );
                marketCache.add(marketToAdd);
            }
        }
    }

    for (const additionalMarket of ensureArray(context.additionalMarket)) {
        const [name, addressString, programIdString] = additionalMarket.split(":");
        const marketToAdd = new mkts.Market(
            context,
            name,
            new solana.PublicKey(addressString),
            new solana.PublicKey(programIdString),
            mkts.MarketSource.COMMANDLINE
        );
        marketCache.add(marketToAdd);
    }

    return marketCache;
}

function parseRetries(retries) {
    if (retries === DEFAULT_RETRY_DELAYS) {
        return retries;
    }

    const allParsed = [];
    for (const toParse of retries.split(",")) {
        if (toParse !== "") {
            const parsed = parseInt(toParse, numbers.DECIMAL_RADIX);
            allParsed.push(parsed);
        }
    }

    return allParsed;
}

function makeTypeSafe(context) {
    context.retries = parseRetries(context.retries);
    if (!ALLOWED_COMMITMENTS.includes(context.commitment)) {
        throw new TypeError(`Unknown commitment type: ${context.commitment} - must be one of [${ALLOWED_COMMITMENTS.join(", ")}]`);
    }

    context.limit = numbers.parseIntWithDefault(context.limit, DEFAULT_LIMIT);
    context.refreshSettlerEvery = numbers.parseIntWithDefault(
        context.refreshSettlerEvery,
        DEFAULT_REFRESH_SETTLER_EVERY
    );

    context.feeProportion = numbers.percentageToProportion(context.feePercentage, DEFAULT_FEE_PERCENTAGE);

    const pollInterval = numbers.parseIntWithDefault(context.pollInterval, DEFAULT_POLL_INTERVAL_SECONDS);
    context.pollIntervalSeconds = pollInterval;
    context.pollIntervalMilliseconds = pollInterval * numbers.MILLISECONDS_TO_SECONDS_FACTOR;

    context.positionSizeProportion = numbers.percentageToProportion(
        context.positionSizePercentage,
        DEFAULT_POSITION_SIZE_PERCENTAGE
    );

    return context;
}

function buildContext(args, customDefaults = {}) {
    if (!args) {
        // Can't easily use asserts in this file.
        throw new TypeError(`Context.buildContext() parameter error - args '${args}' is not defined.`);
    }

    const parsedArgs = argParser(args.slice(IGNORE_ARGV_COMMAND_NAME));
    if (parsedArgs.verbose) {
        parsedArgs.trace = parsedArgs.verbose;
    }

    const intermediate = Object.assign(new Context(), customDefaults, parsedArgs);
    return makeTypeSafe(intermediate);
}

function loadMarket(context, marketName) {
    if (!marketName) {
        return null;
    }

    return context.markets.byName(marketName);
}

async function loadContext(args, customDefaults) {
    const context = buildContext(args, customDefaults);
    log.setLevels({
        trace: context.trace
    });

    context.connection = connector.createConnection(context);

    context.tokenCache = buildTokenCache(context);

    const marketCache = buildMarketCache(context);
    context.markets = new mkts.Markets(marketCache);

    context.currentMarket = await loadMarket(context, context.market);

    cleanupContext(context);

    Object.freeze(context);
    return context;
}

module.exports = {
    Context,
    buildTokenCache,
    buildMarketCache,
    parseRetries,
    buildContext,
    loadContext
};