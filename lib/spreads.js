"use strict";

const assert = require("./assert.js");
const Big = require("big.js");
const numbers = require("../lib/numbers.js");
const serum = require("@project-serum/serum");
const tags = require("../lib/tags.js");
const token = require("../lib/token.js");

class Spread {
    constructor(baseToken, quoteToken, bestBid, bids, bestAsk, asks, mid) {
        assert.parameterType("Spread()", "baseToken", baseToken, token.Token);
        assert.parameterType("Spread()", "quoteToken", quoteToken, token.Token);
        assert.parameterType("Spread()", "bestBid", bestBid, Big);
        assert.parameterType("Spread()", "bestAsk", bestAsk, Big);
        assert.parameterType("Spread()", "mid", mid, Big);

        this.isSpread = true;

        this.baseToken = baseToken;
        this.quoteToken = quoteToken;

        this.bestBid = bestBid;
        this.bestAsk = bestAsk;
        this.mid = mid;
        this.bids = bids;
        this.asks = asks;

        tags.ensureTags(this);

        if (new.target === Spread) {
            Object.freeze(this);
        }
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    toString() {
        return `« ${this.bestBid} / ${this.bestAsk} ${this.quoteToken.symbol} »`;
    }
}

class SpreadFetcher {
    constructor() {
        assert.notConstructingAbstractType(SpreadFetcher, new.target);

        this.isSpreadFetcher = true;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    fetchSpread() {
        assert.throwOnAbstractCall(SpreadFetcher, this.fetchSpread);
    }
}

class MarketSpreadFetcher extends SpreadFetcher {
    constructor(context, market) {
        super();

        assert.parameterTypeIsContext("MarketSpreadFetcher()", "context", context);
        assert.parameterType("MarketSpreadFetcher()", "market", market, serum.Market);

        this.context = context;
        this.market = market;
        this.baseToken = context.tokenCache.bySymbol(market.base);
        this.quoteToken = context.tokenCache.bySymbol(market.quote);

        if (new.target === SpreadFetcher) {
            Object.freeze(this);
        }
    }

    async fetchSpread() {
        // typedLoadBids() returns an array of Order objects, sorted by increasing price.
        const bids = await this.market.typedLoadBids();

        // typedLoadAsks() returns an array or Order objects, sorted by increasing price.
        const asks = await this.market.typedLoadAsks();

        const bestBid = new Big(bids[bids.length - 1].price);
        const bestAsk = new Big(asks[0].price);
        const mid = bestBid.add(bestAsk).div(new Big(numbers.HALF_DIVISOR));

        return new Spread(this.baseToken, this.quoteToken, bestBid, bids, bestAsk, asks, mid);
    }
}

module.exports = {
    Spread,
    SpreadFetcher,
    MarketSpreadFetcher
};