"use strict";

const assert = require("./assert.js");
const ctx = require("./context.js");
const formatter = require("../lib/formatter.js");
const log = require("../lib/log.js");
const numbers = require("../lib/numbers.js");
const serum = require("@project-serum/serum");
const solana = require("@solana/web3.js");
const token = require("../lib/token.js");

class Settler {
    constructor() {
        assert.notConstructingAbstractType(Settler, new.target);

        this.isSettler = true;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    settle() {
        assert.throwOnAbstractCall(Settler, this.settle);
    }
}

class SimulationSettler extends Settler {
    constructor() {
        super();

        if (new.target === SimulationSettler) {
            Object.freeze(this);
        }
    }

    // eslint-disable-next-line class-methods-use-this
    settle() {
        log.print("Simulation - not settling.");

        return Promise.resolve();
    }
}

class AbstractMarketSettler extends Settler {
    constructor(context, owner, market, baseTokenAccount, quoteTokenAccount) {
        super();

        assert.parameterType("AbstractMarketSettler()", "context", context, ctx.Context);
        assert.parameterType("AbstractMarketSettler()", "owner", owner, solana.Account);
        assert.parameterType("AbstractMarketSettler()", "market", market, serum.Market);
        assert.parameterType("AbstractMarketSettler()", "baseTokenAccount", baseTokenAccount, token.TokenAccount);
        assert.parameterType("AbstractMarketSettler()", "quoteTokenAccount", quoteTokenAccount, token.TokenAccount);

        this.context = context;
        this.owner = owner;
        this.market = market;
        this.baseTokenAccount = baseTokenAccount;
        this.quoteTokenAccount = quoteTokenAccount;
    }

    settle() {
        assert.throwOnAbstractCall(AbstractMarketSettler, this.settle);
    }

    format(openOrders) {
        return formatter.openOrders(this.market, openOrders);
    }

    fetchOpenOrders() {
        return this.market.typedFindOpenOrdersAccountsForOwner(this.owner.publicKey);
    }

    settleOpenOrders(openOrders) {
        log.print("Settling:", this.format(openOrders));
        return this.market.typedSettleFunds(this.owner, this.baseTokenAccount, this.quoteTokenAccount, openOrders);
    }
}

class AsyncSettler extends AbstractMarketSettler {
    constructor(context, owner, market, baseTokenAccount, quoteTokenAccount) {
        super(context, owner, market, baseTokenAccount, quoteTokenAccount);

        if (new.target === AsyncSettler) {
            Object.freeze(this);
        }
    }

    async settle() {
        const openOrdersCollection = await this.fetchOpenOrders();
        for (const openOrders of openOrdersCollection) {
            if (!numbers.isZero(openOrders.baseTokenFree) || !numbers.isZero(openOrders.quoteTokenFree)) {
                log.print("Settling:", this.format(openOrders));

                // eslint-disable-next-line no-await-in-loop
                const signature = await this.settleOpenOrders(openOrders);

                log.print("Settle transaction sent:", signature);
            } else {
                log.trace("No need to settle:", this.format(openOrders));
            }
        }
    }
}

class SyncSettler extends AbstractMarketSettler {
    constructor(context, owner, market, baseTokenAccount, quoteTokenAccount) {
        super(context, owner, market, baseTokenAccount, quoteTokenAccount);

        if (new.target === SyncSettler) {
            Object.freeze(this);
        }
    }

    async settle() {
        const openOrdersCollection = await this.fetchOpenOrders();
        for (const openOrders of openOrdersCollection) {
            if (!numbers.isZero(openOrders.baseTokenFree) || !numbers.isZero(openOrders.quoteTokenFree)) {
                log.print("Settling:", this.format(openOrders));

                // eslint-disable-next-line no-await-in-loop
                const signature = await this.settleOpenOrders(openOrders);

                // eslint-disable-next-line no-await-in-loop
                await this.context.connection.waitFor(signature);

                log.print("Settled:", signature);
            } else {
                log.trace("No need to settle:", this.format(openOrders));
            }
        }
    }
}

class AutoRefreshSettler extends Settler {
    constructor(refreshEvery, factory) {
        super();

        assert.parameterTypeOf("Seen()", "refreshEvery", refreshEvery, "number");
        assert.isTrue(refreshEvery >= 1, "Refresh parameter must be at least 1");
        assert.parameterTypeOf("Seen()", "factory", factory, "function");

        this.refreshEvery = refreshEvery;
        this.factory = factory;
        this.counter = 0;
        this.innerSettler = null;
    }

    async settle() {
        if (this.counter === 0) {
            const innerSettler = await this.factory();
            assert.objectType("AutoRefreshSettler.settle()", "innerSettler", innerSettler, Settler);
            this.innerSettler = innerSettler;
        }

        this.innerSettler.settle();

        this.counter = (this.counter + 1) % this.refreshEvery;
    }
}

async function createSettler(context, owner, market) {
    const baseTokenAddresses = await market.typedFindBaseTokenAccountsForOwner(owner.publicKey);
    const quoteTokenAddresses = await market.typedFindQuoteTokenAccountsForOwner(owner.publicKey);

    if (context.simulate) {
        log.trace("Using SimulationSettler");
        return new SimulationSettler();
    } else if (context.asynchronous) {
        log.trace("Using AsyncSettler");
        return new AsyncSettler(context, owner, market, baseTokenAddresses[0], quoteTokenAddresses[0]);
    }

    log.trace("Using SyncSettler");
    return new SyncSettler(context, owner, market, baseTokenAddresses[0], quoteTokenAddresses[0]);
}

module.exports = {
    Settler,
    SimulationSettler,
    AbstractMarketSettler,
    AsyncSettler,
    SyncSettler,
    AutoRefreshSettler,
    createSettler
};