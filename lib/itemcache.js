"use strict";

const assert = require("./assert.js");
const log = require("../lib/log.js");
const solana = require("@solana/web3.js");

function normaliseSymbol(symbol) {
    return symbol.toUpperCase();
}

class ItemCache {
    constructor(itemType, items, unknown) {
        assert.parameterType("ItemCache()", "itemType", itemType, Object);
        assert.parameterTypeIsArrayOfType("ItemCache()", "items", items, itemType);

        this.isItemCache = true;

        this.itemType = itemType;
        this.items = [];
        this.unknown = unknown;

        this._bySymbol = {};
        this._byAddress = {};
        for (const token of items) {
            this.add(token);
        }

        Object.freeze(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    add(item) {
        assert.parameterType("ItemCache.add()", "item", item, this.itemType);

        const normalisedSymbol = normaliseSymbol(item.symbol);
        const existingBySymbol = this._bySymbol[normalisedSymbol];
        if (existingBySymbol) {
            log.warn(`ItemCache adding item - overwriting ${existingBySymbol} with ${item} by address ${item.address}`);
        }
        this._bySymbol[normalisedSymbol] = item;

        const existingByAddress = this._byAddress[item.address];
        if (existingByAddress) {
            log.warn(`ItemCache adding item - overwriting ${existingByAddress} with ${item} by symbol ${item.symbol}`);
        }
        this._byAddress[item.address] = item;

        this.items.push(item);
    }

    bySymbol(symbol) {
        assert.parameterTypeIsString("ItemCache.bySymbol()", "symbol", symbol);

        const normalisedSymbol = normaliseSymbol(symbol);
        return this._bySymbol[normalisedSymbol] || (() => {
            throw new Error(`Unknown item symbol ${symbol}`);
        })();
    }

    bySymbolOrUnknown(symbol) {
        assert.parameterTypeIsString("ItemCache.bySymbol()", "symbol", symbol);

        const normalisedSymbol = normaliseSymbol(symbol);
        return this._bySymbol[normalisedSymbol] || this.unknown;
    }

    byAddress(address) {
        assert.parameterType("ItemCache.byAddress()", "address", address, solana.PublicKey);

        return this._byAddress[address] || (() => {
            throw new Error(`Unknown item address ${address}`);
        })();
    }

    byAddressOrUnknown(address) {
        assert.parameterType("ItemCache.byAddressOrUnknown()", "address", address, solana.PublicKey);

        return this._byAddress[address] || this.unknown;
    }
}

module.exports = {
    ItemCache
};