"use strict";

const Big = require("big.js");
const BufferLayout = require("buffer-layout");
const solana = require("@solana/web3.js");

const PUBLIC_KEY_LENGTH = 32;
const INT_LENGTH = 8;
const TOKENSWAP_PADDING_LENGTH = 16;
const ACCOUNT_PADDING_LENGTH = 93;

const AccountLayout = BufferLayout.struct([
    BufferLayout.blob(PUBLIC_KEY_LENGTH, "mint"),
    BufferLayout.blob(PUBLIC_KEY_LENGTH, "owner"),
    BufferLayout.nu64("amount"),
    BufferLayout.blob(ACCOUNT_PADDING_LENGTH)
]);

function decodeInnerAccount(encodedAccount) {
    const {mint, amount} = AccountLayout.decode(encodedAccount.data);
    return {
        ...encodedAccount,
        mint: new solana.PublicKey(mint),
        amount: new Big(amount)
    };
}

function decodeAccountValue(encodedAccountResult) {
    const account = decodeInnerAccount(encodedAccountResult.account);
    return {
        ...encodedAccountResult,
        publicKey: encodedAccountResult.pubkey,
        account
    };
}

function decodeAccountResponse(encoded) {
    const results = encoded.value;
    const decoded = results.map((result) => decodeAccountValue(result));

    return {
        ...encoded,
        value: decoded
    };
}

const TokenSwapLayout = BufferLayout.struct([
    BufferLayout.u8("isInitialized"),
    BufferLayout.u8("nonce"),
    BufferLayout.blob(PUBLIC_KEY_LENGTH, "tokenProgramId"),
    BufferLayout.blob(PUBLIC_KEY_LENGTH, "tokenAccountA"),
    BufferLayout.blob(PUBLIC_KEY_LENGTH, "tokenAccountB"),
    BufferLayout.blob(PUBLIC_KEY_LENGTH, "tokenPool"),
    BufferLayout.blob(PUBLIC_KEY_LENGTH, "mintA"),
    BufferLayout.blob(PUBLIC_KEY_LENGTH, "mintB"),
    BufferLayout.blob(PUBLIC_KEY_LENGTH, "feeAccount"),
    BufferLayout.u8("curveType"),
    BufferLayout.blob(INT_LENGTH, "tradeFeeNumerator"),
    BufferLayout.blob(INT_LENGTH, "tradeFeeDenominator"),
    BufferLayout.blob(INT_LENGTH, "ownerTradeFeeNumerator"),
    BufferLayout.blob(INT_LENGTH, "ownerTradeFeeDenominator"),
    BufferLayout.blob(INT_LENGTH, "ownerWithdrawFeeNumerator"),
    BufferLayout.blob(INT_LENGTH, "ownerWithdrawFeeDenominator"),
    BufferLayout.blob(TOKENSWAP_PADDING_LENGTH, "padding")
]);

module.exports = {
    AccountLayout,
    decodeInnerAccount,
    decodeAccountResponse,
    TokenSwapLayout
};