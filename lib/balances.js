"use strict";

const assert = require("./assert.js");
const Big = require("big.js");
const ctx = require("./context.js");
const log = require("../lib/log.js");
const numbers = require("../lib/numbers.js");
const serum = require("@project-serum/serum");
const solana = require("@solana/web3.js");
const tags = require("../lib/tags.js");
const token = require("../lib/token.js");

async function _combineAccountTokenBalances(connection, tokenAccounts) {
    const accountBalances = [];
    for (const tokenAccount of tokenAccounts) {
        accountBalances.push(connection.getTokenAccountBalance(tokenAccount.publicKey));
    }

    let balance = numbers.Zero;
    for (const accountBalance of await Promise.all(accountBalances)) {
        const addition = numbers.createBigFromNumberAndDecimals(
            accountBalance.value.amount, accountBalance.value.decimals
        );
        balance = balance.add(addition);
    }

    return balance;
}

async function _fetchOpenOrdersBalances(market, owner) {
    const result = {
        base: numbers.Zero,
        quote: numbers.Zero
    };

    const openOrdersCollection = await market.typedFindOpenOrdersAccountsForOwner(owner.publicKey);
    for (const openOrders of openOrdersCollection) {
        result.base = result.base.add(new Big(market.baseSplSizeToNumber(openOrders.baseTokenTotal)));
        result.quote = result.quote.add(new Big(market.quoteSplSizeToNumber(openOrders.quoteTokenTotal)));
    }

    return result;
}

class BalancePair {
    constructor(base, quote) {
        assert.parameterType("BalancePair()", "base", base, token.TokenValue);
        assert.parameterType("BalancePair()", "quote", quote, token.TokenValue);

        this.isBalancePair = true;

        this.base = base;
        this.quote = quote;

        tags.ensureTags(this);

        Object.freeze(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    toString() {
        return `« ${this.base}, ${this.quote} »`;
    }
}

class Balance {
    constructor(actualTotal, total, settled, unsettled) {
        this.isBalance = true;

        assert.parameterType("Balance()", "actualTotal", actualTotal, BalancePair);
        assert.parameterType("Balance()", "total", total, BalancePair);
        assert.parameterType("Balance()", "settled", settled, BalancePair);
        assert.parameterType("Balance()", "unsettled", unsettled, BalancePair);

        this.actualTotal = actualTotal;
        this.total = total;
        this.settled = settled;
        this.unsettled = unsettled;

        tags.ensureTags(this);

        Object.freeze(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    toString() {
        return `« ${this.total.base}, ${this.total.quote} »`;
    }
}

class BalanceFetcher {
    constructor() {
        assert.notConstructingAbstractType(BalanceFetcher, new.target);

        this.isBalanceFetcher = true;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    fetchBalance() {
        assert.throwOnAbstractCall(BalanceFetcher, this.fetchBalance);
    }
}

class SyncBalanceFetcher extends BalanceFetcher {
    constructor(context, market, owner, baseTokenAccounts, quoteTokenAccounts) {
        super();

        assert.parameterType("SyncBalanceFetcher()", "context", context, ctx.Context);
        assert.parameterType("SyncBalanceFetcher()", "market", market, serum.Market);
        assert.parameterType("SyncBalanceFetcher()", "owner", owner, solana.Account);
        // eslint-disable-next-line max-len
        assert.parameterTypeIsArrayOfType("SyncBalanceFetcher()", "baseTokenAccounts", baseTokenAccounts, token.TokenAccount);
        // eslint-disable-next-line max-len
        assert.parameterTypeIsArrayOfType("SyncBalanceFetcher()", "quoteTokenAccounts", quoteTokenAccounts, token.TokenAccount);

        this.market = market;
        this.owner = owner;

        this.baseToken = context.tokenCache.bySymbol(market.base);
        this.quoteToken = context.tokenCache.bySymbol(market.quote);

        this.baseTokenAccounts = baseTokenAccounts;
        this.quoteTokenAccounts = quoteTokenAccounts;

        this.context = context;

        if (new.target === SyncBalanceFetcher) {
            Object.freeze(this);
        }
    }

    async fetchBalance() {
        const unsettledValues = await _fetchOpenOrdersBalances(this.market, this.owner);

        const baseBalance = await _combineAccountTokenBalances(this.context.connection, this.baseTokenAccounts);
        const baseTotal = new token.TokenValue(
            this.baseToken,
            baseBalance.plus(unsettledValues.base),
            this.market._baseSplTokenDecimals
        );
        const baseSettled = new token.TokenValue(
            this.baseToken,
            baseBalance,
            this.market._baseSplTokenDecimals
        );
        const baseUnsettled = new token.TokenValue(
            this.baseToken,
            unsettledValues.base,
            this.market._baseSplTokenDecimals
        );

        const quoteBalance = await _combineAccountTokenBalances(this.context.connection, this.quoteTokenAccounts);
        const quoteTotal = new token.TokenValue(
            this.quoteToken,
            quoteBalance.plus(unsettledValues.quote),
            this.market._quoteSplTokenDecimals
        );
        const quoteSettled = new token.TokenValue(
            this.quoteToken,
            quoteBalance,
            this.market._quoteSplTokenDecimals
        );
        const quoteUnsettled = new token.TokenValue(
            this.quoteToken,
            unsettledValues.quote,
            this.market._quoteSplTokenDecimals
        );

        const total = new BalancePair(baseTotal, quoteTotal);
        const settled = new BalancePair(baseSettled, quoteSettled);
        const unsettled = new BalancePair(baseUnsettled, quoteUnsettled);

        return new Balance(total, total, settled, unsettled);
    }
}

class FixedBalanceFetcher extends BalanceFetcher {
    constructor(balanceFetcher, baseBalance, quoteBalance) {
        super();

        assert.parameterType("FixedBalanceFetcher()", "balanceFetcher", balanceFetcher, BalanceFetcher);

        // 'baseBalance' can be undefined or a Big, nothing else.
        if (baseBalance) {
            assert.parameterType("FixedBalanceFetcher()", "baseBalance", baseBalance, Big);
        }

        // 'quoteBalance' can be undefined or a Big, nothing else.
        if (quoteBalance) {
            assert.parameterType("FixedBalanceFetcher()", "quoteBalance", quoteBalance, Big);
        }

        this.innerBalanceFetcher = balanceFetcher;
        this.baseBalance = baseBalance ?
            new Big(baseBalance) :
            null;
        this.quoteBalance = quoteBalance ?
            new Big(quoteBalance) :
            null;

        if (new.target === FixedBalanceFetcher) {
            Object.freeze(this);
        }
    }

    async fetchBalance() {
        const original = await this.innerBalanceFetcher.fetchBalance();

        const baseTotal = this.baseBalance ?
            new token.TokenValue(original.total.base.token, this.baseBalance, original.total.base.decimals) :
            original.total.base;
        const quoteTotal = this.quoteBalance ?
            new token.TokenValue(original.total.quote.token, this.quoteBalance, original.total.quote.decimals) :
            original.total.quote;
        const total = new BalancePair(baseTotal, quoteTotal);

        const baseSettled = this.baseBalance ?
            baseTotal :
            original.settled.base;
        const quoteSettled = this.quoteBalance ?
            quoteTotal :
            original.settled.quote;
        const settled = new BalancePair(baseSettled, quoteSettled);

        const baseUnsettled = this.baseBalance ?
            new token.TokenValue(original.total.base.token, numbers.Zero, original.total.base.decimals) :
            original.unsettled.base;
        const quoteUnsettled = this.quoteBalance ?
            new token.TokenValue(original.total.quote.token, numbers.Zero, original.total.quote.decimals) :
            original.unsettled.quote;
        const unsettled = new BalancePair(baseUnsettled, quoteUnsettled);

        return new Balance(original.total, total, settled, unsettled);
    }
}

class AddingBalanceFetcher extends BalanceFetcher {
    constructor(balanceFetcher, additionalBase, additionalQuote) {
        super();

        assert.parameterType("AddingBalanceFetcher()", "balanceFetcher", balanceFetcher, BalanceFetcher);
        assert.parameterType("AddingBalanceFetcher()", "additionalBase", additionalBase, Big);
        assert.parameterType("AddingBalanceFetcher()", "additionalQuote", additionalQuote, Big);

        this.innerBalanceFetcher = balanceFetcher;
        this.additionalBase = additionalBase;
        this.additionalQuote = additionalQuote;

        if (new.target === AddingBalanceFetcher) {
            Object.freeze(this);
        }
    }

    async fetchBalance() {
        const original = await this.innerBalanceFetcher.fetchBalance();
        const newTotal = new BalancePair(
            original.total.base.add(this.additionalBase),
            original.total.quote.add(this.additionalQuote)
        );
        const newSettled = new BalancePair(
            original.settled.base.add(this.additionalBase),
            original.settled.quote.add(this.additionalQuote)
        );

        return new Balance(original.total, newTotal, newSettled, original.unsettled);
    }
}

class MultiplyingBalanceFetcher extends BalanceFetcher {
    constructor(balanceFetcher, adjustmentFactor) {
        super();

        assert.parameterType("MultiplyingBalanceFetcher()", "balanceFetcher", balanceFetcher, BalanceFetcher);
        assert.parameterType("MultiplyingBalanceFetcher()", "adjustmentFactor", adjustmentFactor, Big);

        this.innerBalanceFetcher = balanceFetcher;
        this.adjustmentFactor = adjustmentFactor;

        if (new.target === MultiplyingBalanceFetcher) {
            Object.freeze(this);
        }
    }

    async fetchBalance() {
        const original = await this.innerBalanceFetcher.fetchBalance();
        const newTotal = new BalancePair(
            original.total.base.multiply(this.adjustmentFactor),
            original.total.quote.multiply(this.adjustmentFactor)
        );
        const newSettled = new BalancePair(
            original.settled.base.multiply(this.adjustmentFactor),
            original.settled.quote.multiply(this.adjustmentFactor)
        );

        return new Balance(original.total, newTotal, newSettled, original.unsettled);
    }
}

class SpreadMultiplyingBalanceFetcher extends BalanceFetcher {
    constructor(balanceFetcher, market, spreadAdjustmentFactor) {
        super();

        assert.parameterType("SpreadMultiplyingBalanceFetcher()", "balanceFetcher", balanceFetcher, BalanceFetcher);
        assert.parameterType("SpreadMultiplyingBalanceFetcher()", "market", market, serum.Market);
        // eslint-disable-next-line max-len
        assert.parameterType("SpreadMultiplyingBalanceFetcher()", "spreadAdjustmentFactor", spreadAdjustmentFactor, Big);

        this.innerBalanceFetcher = balanceFetcher;
        this.market = market;
        this.spreadAdjustmentFactor = spreadAdjustmentFactor;

        if (new.target === SpreadMultiplyingBalanceFetcher) {
            Object.freeze(this);
        }
    }

    async fetchBalance() {
        const original = await this.innerBalanceFetcher.fetchBalance();
        const spread = await this.market.fetchSpread();
        const baseAdjustment = this.spreadAdjustmentFactor;
        const quoteAdjustment = spread.mid.times(this.spreadAdjustmentFactor);
        const newTotal = new BalancePair(
            original.total.base.add(baseAdjustment),
            original.total.quote.add(quoteAdjustment)
        );
        const newSettled = new BalancePair(
            original.settled.base.add(baseAdjustment),
            original.settled.quote.add(quoteAdjustment)
        );

        return new Balance(original.total, newTotal, newSettled, original.unsettled);
    }
}

function createFetcher(context, market, owner, accountTracker) {
    const opts = context || {};
    const balanceFetcher = new SyncBalanceFetcher(
        context, market, owner, accountTracker.baseTokenAccounts, accountTracker.quoteTokenAccounts
    );
    if (opts.baseBalance || opts.quoteBalance) {
        log.print(`Setting balances to: ${opts.baseBalance} / ${opts.quoteBalance}`);
        return new FixedBalanceFetcher(balanceFetcher, opts.baseBalance, opts.quoteBalance);
    } else if (opts.additionalBaseBalance || opts.additionalQuoteBalance) {
        const additionalBaseBalanceValue = opts.additionalBaseBalance ?
            new Big(opts.additionalBaseBalance) :
            numbers.Zero;
        const additionalQuoteBalanceValue = opts.additionalQuoteBalance ?
            new Big(opts.additionalQuoteBalance) :
            numbers.Zero;
        log.print(`Adding: ${additionalBaseBalanceValue} / ${additionalQuoteBalanceValue} to real balances`);
        return new AddingBalanceFetcher(balanceFetcher, additionalBaseBalanceValue, additionalQuoteBalanceValue);
    } else if (opts.balanceMultiplicationFactor) {
        const balanceMultiplicationFactorValue = new Big(opts.balanceMultiplicationFactor);
        log.print(`Multiplying balances by: ${balanceMultiplicationFactorValue} times.`);
        return new MultiplyingBalanceFetcher(balanceFetcher, balanceMultiplicationFactorValue);
    } else if (opts.spreadAdjustmentFactor) {
        const spreadAdjustmentFactor = new Big(opts.spreadAdjustmentFactor);
        log.print(`Adjusting balances by: ${spreadAdjustmentFactor} base and ${spreadAdjustmentFactor} times quote-price quote.`);
        return new SpreadMultiplyingBalanceFetcher(balanceFetcher, market, spreadAdjustmentFactor);
    }

    return balanceFetcher;
}

module.exports = {
    BalancePair,
    Balance,
    BalanceFetcher,
    SyncBalanceFetcher,
    FixedBalanceFetcher,
    AddingBalanceFetcher,
    MultiplyingBalanceFetcher,
    SpreadMultiplyingBalanceFetcher,
    createFetcher
};