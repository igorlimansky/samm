"use strict";

const array = require("./array.js");
const assert = require("./assert.js");
const ctx = require("./context.js");
const log = require("../lib/log.js");
const markets = require("./markets.js");
const numbers = require("./numbers.js");
const orderfetcher = require("../lib/orderfetcher.js");
const ordertracker = require("../lib/ordertracker.js");
const sleep = require("../lib/sleep.js");

const DEFAULT_SLEEP_INCREMENT = 2;
const DEFAULT_TIMEOUT_SECONDS = 180;

class OrderWaiter {
    constructor() {
        assert.notConstructingAbstractType(OrderWaiter, new.target);

        this.isOrderWaiter = true;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    waitForOrders() {
        assert.throwOnAbstractCall(OrderWaiter, this.waitForOrders);
    }
}

class SimulationOrderWaiter extends OrderWaiter {
    constructor() {
        super();

        if (new.target === SimulationOrderWaiter) {
            Object.freeze(this);
        }
    }

    // eslint-disable-next-line class-methods-use-this
    waitForOrders() {
        log.print("Simulation - not waiting for orders");
    }
}

class SyncOrderWaiter extends OrderWaiter {
    constructor(context, market, orderFetcher, orderTracker, sleepIncrement, timeoutSeconds) {
        super();

        assert.parameterType("SyncOrderWaiter()", "context", context, ctx.Context);
        assert.parameterType("SyncOrderWaiter()", "market", market, markets.Market);
        assert.parameterType("SyncOrderWaiter()", "orderFetcher", orderFetcher, orderfetcher.OrderFetcher);
        assert.parameterType("SyncOrderWaiter()", "orderTracker", orderTracker, ordertracker.OrderTracker);
        assert.parameterTypeOf("SyncOrderWaiter()", "sleepIncrement", sleepIncrement, "number");
        assert.parameterTypeOf("SyncOrderWaiter()", "timeoutSeconds", timeoutSeconds, "number");

        this.context = context;
        this.market = market;
        this.orderFetcher = orderFetcher;
        this.orderTracker = orderTracker;
        this.sleepIncrement = sleepIncrement;
        this.timeoutSeconds = timeoutSeconds;

        if (new.target === SyncOrderWaiter) {
            Object.freeze(this);
        }
    }

    // eslint-disable-next-line max-statements
    async waitForOrders(orders) {
        const start = new Date();
        const millisecondsTimeout = this.timeoutSeconds * numbers.MILLISECONDS_TO_SECONDS_FACTOR;
        let sleepSeconds = 1;
        const allIds = orders.map((order) => order.clientId);

        const trackingOk = this.orderTracker.allIdsTracked(orders.map((order) => order.clientId));
        assert.isTrue(trackingOk, `Inconsistent tracking IDs (should never happen). Placed IDs: [${allIds.map((id) => id.toString()).join(", ")}]. Tracked IDs: [${this.orderTracker.trackedIds.map((id) => id.toString()).join(", ")}]`);

        do {
            log.trace("Waiting for IDs:", allIds.map((id) => id.toString()).join(", "));

            // eslint-disable-next-line no-await-in-loop
            const newlyAdded = await this.orderFetcher.fetchExistingOrders();
            newlyAdded.map((order) => array.remove(allIds, order.clientId, "eq"));
            log.trace("After fetching existing orders, waiting for IDs:", allIds.map((id) => id.toString()).join(", "));

            if (allIds.length !== 0) {
                // eslint-disable-next-line no-await-in-loop
                const events = await this.market.typedLoadEventQueue();
                events.map((event) => array.remove(allIds, event.clientOrderId, "eq"));
                log.trace("After checking events, waiting for IDs:", allIds.map((id) => id.toString()).join(", "));
            }

            if (allIds.length !== 0) {
                // eslint-disable-next-line no-await-in-loop
                const fills = await this.market.typedLoadFills(this.context.limit);
                fills.map((fill) => array.remove(allIds, fill.clientOrderId, "eq"));
                log.trace("After checking fills, waiting for IDs:", allIds.map((id) => id.toString()).join(", "));
            }

            if (allIds.length !== 0) {
                const timeTakeSoFar = new Date() - start;
                if (timeTakeSoFar > millisecondsTimeout) {
                    const secondsTakenSoFar = numbers.toInt(timeTakeSoFar / numbers.MILLISECONDS_TO_SECONDS_FACTOR);
                    throw new Error(`Timed out after ${secondsTakenSoFar} seconds waiting for orders.`);
                }

                // eslint-disable-next-line no-await-in-loop
                await sleep.sleepForSeconds(sleepSeconds);
                sleepSeconds += this.sleepIncrement;
            }
        } while (allIds.length !== 0);

        const end = new Date() - start;
        log.trace(`Time spent waiting for orders to appear: ${end}ms.`);
    }
}

function createOrderWaiter(context, market, orderFetcher, orderTracker) {
    if (context.simulate) {
        log.trace("Using SimulationOrderWaiter");
        return new SimulationOrderWaiter();
    }

    const waitFor = context.orderWaitPollIncrement ?? DEFAULT_SLEEP_INCREMENT;
    const timeoutSeconds = context.orderWaitTimeoutSeconds ?? DEFAULT_TIMEOUT_SECONDS;

    log.trace(`Using SyncOrderWaiter with waiting increment of ${waitFor} seconds and a timeout of ${timeoutSeconds} seconds.`);
    return new SyncOrderWaiter(context, market, orderFetcher, orderTracker, waitFor, timeoutSeconds);
}

module.exports = {
    OrderWaiter,
    SimulationOrderWaiter,
    SyncOrderWaiter,
    createOrderWaiter
};