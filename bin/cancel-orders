#! /usr/bin/env node

"use strict";

const ctx = require("../lib/context.js");
const log = require("../lib/log.js");
const ordercanceller = require("../lib/ordercanceller.js");
const orderfetcher = require("../lib/orderfetcher.js");

async function main() {
    const context = await ctx.loadContext(process.argv);
    const owner = context.loadAccount();
    const market = context.currentMarketOrThrow;

    const orderFetcher = orderfetcher.createOrderFetcher(context, market, owner);
    const orders = await orderFetcher.fetchExistingOrders();
    if (orders.length === 0) {
        log.print(`No open orders in market ${market.name} for owner ${owner.publicKey}`);
    } else {
        const orderCanceller = ordercanceller.createOrderCanceller(context, market, owner);
        await orderCanceller.cancelOrders(orders);
    }
}

main().catch((ex) => log.critical(ex));