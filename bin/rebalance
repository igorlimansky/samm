#! /usr/bin/env node

"use strict";

const accounttracker = require("../lib/accounttracker.js");
const balances = require("../lib/balances.js");
const Big = require("big.js");
const ctx = require("../lib/context.js");
const log = require("../lib/log.js");
const numbers = require("../lib/numbers.js");
const orderplacer = require("../lib/orderplacer.js");
const token = require("../lib/token.js");

// eslint-disable-next-line max-statements, max-lines-per-function
async function main() {
    const context = await ctx.loadContext(process.argv);
    const owner = context.loadAccount();
    const market = context.currentMarketOrThrow;

    const spread = await market.fetchSpread();
    log.print(`Spread: ${spread}`);
    log.print(`Mid price: ${spread.mid} ${spread.quoteToken.symbol}`);

    const expectedRatio = numbers.One.div(spread.mid);
    log.print(`Ratio should be: ${expectedRatio}`);

    const accountTracker = await accounttracker.createAccountTracker(context, market, owner);
    const balanceFetcher = balances.createFetcher(context, market, owner, accountTracker);
    const bal = await balanceFetcher.fetchBalance();
    log.print(`Base balance: ${bal.actualTotal.base}`);
    log.print(`Quote balance: ${bal.actualTotal.quote}`);

    if (numbers.isZero(bal.actualTotal.base.value) &&
        numbers.isZero(bal.actualTotal.quote.value)) {
        log.error("No balances to swap - both base and quote balances are zero.");
        return;
    }

    const actualRatio = bal.actualTotal.quote.value.eq(0) ?
        "infinity" :
        bal.actualTotal.base.value.div(bal.actualTotal.quote.value);
    log.print(`Ratio is: ${actualRatio}`);

    const baseInQuoteTerms = spread.mid.times(bal.actualTotal.base.value);
    log.print(`Base in quote terms is: ${baseInQuoteTerms}`);

    const totalInQuoteTerms = bal.actualTotal.quote.add(baseInQuoteTerms);
    log.print(`Total in quote terms is: ${totalInQuoteTerms}`);

    const desiredQuote = totalInQuoteTerms.divide(new Big(numbers.HALF_DIVISOR));
    const desiredBase = new token.TokenValue(
        bal.actualTotal.base.token,
        totalInQuoteTerms.value.div(numbers.HALF_DIVISOR).times(expectedRatio),
        bal.actualTotal.base.decimals
    );
    log.print(`Desired base value: ${desiredBase}`);
    log.print(`Desired quote value: ${desiredQuote}`);

    const changeInBase = desiredBase.subtract(bal.actualTotal.base);
    log.print(`Change in base: ${changeInBase}`);

    let order = null;
    if (changeInBase.value.gt(numbers.Zero)) {
        log.print(`Buy ${changeInBase}`);
        const quoteTokenAccounts = await market.typedFindQuoteTokenAccountsForOwner(owner.publicKey);
        const roundedSize = changeInBase.roundQuantity(changeInBase.value);
        const price = context.useMidPrice ?
            spread.mid :
            spread.bestBid;
        order = {
            owner,
            payer: quoteTokenAccounts[0],
            side: "buy",
            price: Number(price),
            size: Number(roundedSize),
            orderType: "limit"
        };
    } else {
        log.print(`Sell ${changeInBase}`);
        const baseTokenAccounts = await market.typedFindBaseTokenAccountsForOwner(owner.publicKey);
        const roundedSize = changeInBase.roundQuantity(numbers.negate(changeInBase.value));
        const price = context.useMidPrice ?
            spread.mid :
            spread.bestAsk;
        order = {
            owner,
            payer: baseTokenAccounts[0],
            side: "sell",
            price: Number(price),
            size: Number(roundedSize),
            orderType: "limit"
        };
    }

    log.print("Order:", order);

    const orderPlacer = orderplacer.createOrderPlacer(context, market);
    await orderPlacer.placeOrders([order]);
}

main().catch((ex) => log.critical(ex));