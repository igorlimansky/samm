# 💉 Serum Automated Market Maker

## ⚠️ Warning

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


## 💉 What Is SAMM?

Serum Automated Market Maker (SAMM) is a [constant-product](docs/ConstantProduct.md) market making bot for [Serum](https://projectserum.com/).

* It’s a market-making bot.
* It targets a specified Serum market.
* It provides post-only orders for both sides of the market (both bid and ask).
* It uses a constant-product function (similar to [Uniswap](https://uniswap.org/), but with some essential changes) to calculate the price for bids and asks.


## 🗺️ Overview

SAMM places a buy post-only order and a sell post-only order on a Serum market. Every `pollInterval` (30 seconds by default), SAMM checks to see if either order has been filled. If they haven’t, it does nothing.

If an order has been filled:
1. **The other order is cancelled.** It’s possible - although unlikely - that both orders were filled and there are no orders to cancel.
2. **The new balances are assessed,** Since an order has been filled, the balances will have changed - some of the `base` will have been traded for some of the `quote`, or some of the `quote` will have been traded for some of the `base`. SAMM needs the updated balances for the constant product calculation.
3. **New prices are calculated.** The constant product calculation determines what ‘fair’ price should be charged for portion of the `base` or `quote` token. Check out the [constant product](docs/ConstantProduct.md) document for more details.
4. **New orders are built.** Once SAMM has the new prices and sizes, it does a few extra checks. For instance, it checks that the price is the right side of the spread. If a spread is `« lowprice / highprice »`, SAMM will never try to sell lower than the `lowprice`, nor buy higher than the `highprice`. In those cases SAMM shifts the price so that it is within the spread. SAMM also checks to ensure it has sufficient tokens for the size of the order, since some parameters can lead to odd situations.
5. **The new orders are placed.** There’s nothing more interesting to describe about this bit.

SAMM then sleeps for another `pollInterval` and this process then repeats.


## 🏃‍ Running SAMM

**Check out the [Quickstart](docs/Quickstart.md) to get up and running.**

The easiest way to run SAMM is to use the provided container images, using docker, podman, kubernetes or similar. The [Quickstart](docs/Quickstart.md) should be enough get you started, but you can configure and run it how you choose.


## 🤸 Commands

One of the [Quickstart](docs/Quickstart.md) steps sets up `samm` and `sammd` scripts to save typing out the container name and parameters. If you’ve done that, you can then run commands like:

* `samm balance --market BTC/USDT`
This will show the amount of SOL, BTC and USDT you have in those Solana accounts.

* `samm spread --market BTC/USDT`
This will show the current spread - the best bid and best ask - one the specified market.

* `samm orders --market BTC/USDT`
This will show all open orders your account has live on that Serum market.

Check out the full [command reference](docs/Commands.md).


## Contact

The quickest and easiest way to contact me is probably Twitter - I'm [@OpinionatedGeek](https://twitter.com/OpinionatedGeek/).